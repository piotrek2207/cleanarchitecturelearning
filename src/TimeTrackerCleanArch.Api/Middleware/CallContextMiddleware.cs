﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging;
using Shared.Messaging.Authorization;

namespace TimeTrackerCleanArch.Api.Middleware;

public class CallContextMiddleware : IMiddleware
{
    private const string ProjectIdHeader = "X-Project-Id";

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        var callContext = context.RequestServices.GetRequiredService<CallContext>();
        callContext.SetCorrelationId(Guid.NewGuid());

        if (context.User.Identity?.IsAuthenticated == true)
        {
            var userClaim = context.User.Claims.FirstOrDefault(x => x.Type == CustomUserClaimTypes.UserId);
            if (!string.IsNullOrEmpty(userClaim?.Value))
            {
                callContext.SetUserId(Guid.Parse(userClaim.Value));
            }
        }

        if (context.Request.Headers.TryGetValue(ProjectIdHeader, out var headerValue))
        {
            var projectId = Guid.Parse(headerValue);
            callContext.SetProjectId(projectId);
        }

        await next(context);
    }
}