using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shared.Messaging.Bootstrap;
using TimeTracker.Authorization.Application.Commands.RegisterUser;
using TimeTracker.Authorization.Bootstrap;
using TimeTrackerCleanArch.Application.Bootstrap;
using TimeTrackerCleanArch.Application.Project.CreateProject;
using TimeTrackerCleanArch.Domain.Project.Events;
using TimeTrackerCleanArch.Infrastructure.Bootstrap;

namespace TimeTrackerCleanArch.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers().AddNewtonsoftJson();

        services.AddCQRS().InMemory().WithDefaultPipelines();
        services.AddTimeTrackerAuthorization(Configuration);
        services.AddApplication();
        services.AddInfrastructure();
        services.RegisterEventForwarder();
        services.AddMediatR(x =>
        {
            x.RegisterServicesFromAssemblyContaining<RegisterSystemUserCommand>();
            x.RegisterServicesFromAssemblyContaining<CreateProject>();
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}