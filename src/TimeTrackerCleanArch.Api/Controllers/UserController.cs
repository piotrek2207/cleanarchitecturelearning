﻿using Microsoft.AspNetCore.Mvc;
using Shared.Messaging.InProc.Commands;
using Shared.Messaging.InProc.Queries;
using TimeTracker.Authorization.Controllers;

namespace TimeTrackerCleanArch.Api.Controllers;

[Route("api/user")]
[ApiController]
public class UserController : UserControllerBase
{
    public UserController(
        ICommandDispatcher commandDispatcher, 
        IQueryDispatcher queryDispatcher) : base(commandDispatcher, queryDispatcher)
    {
    }
}