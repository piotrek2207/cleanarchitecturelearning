﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TimeTrackerCleanArch.Application.Project.CreateProject;
using TimeTrackerCleanArch.Application.Project.DeactivateProject;
using TimeTrackerCleanArch.Application.Project.GetProject;
using Shared.Messaging.InProc.Queries;
using Shared.Messaging.InProc.Commands;

namespace TimeTrackerCleanArch.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class ProjectController : ControllerBase
{
    private readonly ICommandDispatcher _commandDispatcher;
    private readonly IQueryDispatcher _queryDispatcher;

    public ProjectController(ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
    {
        _commandDispatcher = commandDispatcher;
        _queryDispatcher = queryDispatcher;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetProject([FromRoute] Guid id)
    {
        var query = new GetProject(id);
        var project = await _queryDispatcher.DispatchAsync(query);

        return Ok(project);
    }

    [HttpPost("create")]
    public async Task<IActionResult> CreateProject([FromBody] CreateProject command)
    {
        await _commandDispatcher.DispatchAsync(command);

        return NoContent();
    }

    [HttpPost("deactivate")]
    public async Task<IActionResult> DeactivateProject([FromBody] DeactivateProject command)
    {
        await _commandDispatcher.DispatchAsync(command);

        return NoContent();
    }
}