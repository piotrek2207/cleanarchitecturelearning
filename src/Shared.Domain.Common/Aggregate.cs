﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared.Domain.Common;

public abstract class Aggregate
{
    private readonly Queue<IAggregateEvent> _uncommittedEvents = new Queue<IAggregateEvent>();

    public Guid Id { get; protected set; }
    public long Version { get; protected set; }

    protected Aggregate() {}

    protected Aggregate(Guid id)
    {
        Id = id;
        Version = 0;
    }

    protected void Enqueue(AggregateEvent @event)
    {
        Version++;
        @event.Version = Version;
        _uncommittedEvents.Enqueue(@event);
    }

    public ICollection<IAggregateEvent> DequeueAllUncommittedEvents()
    {
        var dequeuedEvents = _uncommittedEvents.ToArray();

        _uncommittedEvents.Clear();

        return dequeuedEvents.ToList();
    }
}