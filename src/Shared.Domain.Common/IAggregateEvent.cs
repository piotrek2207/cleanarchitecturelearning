﻿using System;

namespace Shared.Domain.Common;

public interface IAggregateEvent : IEvent
{
    Guid AggregateId { get; }
    long Version { get; }
}

public abstract class AggregateEvent : IAggregateEvent
{
    public Guid AggregateId { get; }
    public long Version { get; set; }

    protected AggregateEvent(Guid aggregateId)
    {
        AggregateId = aggregateId;
    }
}