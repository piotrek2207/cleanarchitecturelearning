﻿using MediatR;

namespace Shared.Domain.Common;

public interface IEvent : INotification
{
}