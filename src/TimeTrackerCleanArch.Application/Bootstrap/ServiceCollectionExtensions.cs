﻿using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Extensions;
using TimeTrackerCleanArch.Application.Project.CreateProject;
using TimeTrackerCleanArch.Application.Project.GetProject;

namespace TimeTrackerCleanArch.Application.Bootstrap;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.RegisterCommandHandlersFromAssemblyContaining<CreateProject>();
        services.RegisterQueryHandlersFromAssemblyContaining<GetProject>();

        return services;
    }
}