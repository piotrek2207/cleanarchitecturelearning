﻿using System;

namespace TimeTrackerCleanArch.Application.Exceptions;

public class EntityNotFoundException<T> : Exception
{
    public string EntityId { get; private set; }

    private EntityNotFoundException(string entityId) 
        : base($"Entity of type {typeof(T).Name} with ID {entityId} was not found")
    {
        EntityId = entityId;
    }

    public static EntityNotFoundException<T> Create(Guid id)
    {
        return new EntityNotFoundException<T>(id.ToString());
    }
}