﻿using System;

namespace TimeTrackerCleanArch.Application.Exceptions;

public class EventStreamIncorrectVersionException : Exception
{
    public Guid StreamId { get; }
    public string StreamType { get; }

    private EventStreamIncorrectVersionException(string message, Guid streamId, string streamType) : base(message)
    {
        StreamId = streamId;
        StreamType = streamType;
    }

    public static EventStreamIncorrectVersionException CreteFor<TStream>(Guid streamId, long version)
    {
        var streamType = typeof(TStream).FullName;
        var message = $"Cannot append stream {streamId} of type {streamType} with event of version {version}";
        return new EventStreamIncorrectVersionException(message, streamId, streamType);
    }
}