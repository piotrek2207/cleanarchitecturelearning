﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Shared.Messaging.Events;
using TimeTracker.Authorization.Contracts.User;

namespace TimeTrackerCleanArch.Application.ExternalDependencies
{
    internal class UserEventHandler : IEventHandler<UserRegistered>
    {
        private readonly ILogger<UserEventHandler> _logger;

        public UserEventHandler(ILogger<UserEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(UserRegistered @event, CancellationToken cancellationToken)
        {
            _logger.LogInformation("User {UserId} created", @event.AggregateId);
            return Task.CompletedTask;
        }
    }
}
