﻿using System;
using Shared.Messaging.InProc.Commands;

namespace TimeTrackerCleanArch.Application.Tasks.CreateTask;

public class CreateTask : ICommand
{
    public Guid Id { get; private set; }
    public string Title { get; }
    public string Description { get; }
    public Guid ProjectId { get; }
    public TimeSpan TimeRemaining { get; }

    public CreateTask(Guid id, string title, string description, Guid projectId, TimeSpan timeRemaining)
    {
        Id = id;
        Title = title;
        Description = description;
        ProjectId = projectId;
        TimeRemaining = timeRemaining;
    }
}