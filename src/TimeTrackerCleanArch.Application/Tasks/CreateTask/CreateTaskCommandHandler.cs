﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Commands;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Domain.Task;

namespace TimeTrackerCleanArch.Application.Tasks.CreateTask;

public class CreateTaskCommandHandler : ICommandHandler<CreateTask>
{
    private readonly IRepository<TaskAggregate> _repository;

    public CreateTaskCommandHandler(IRepository<TaskAggregate> repository)
    {
        _repository = repository;
    }

    public async Task HandleAsync(CreateTask command)
    {
        var task = new TaskAggregate(
            id: command.Id,
            title: command.Title,
            description: command.Description,
            status: TaskStatusEnum.ToDo,
            projectId: command.ProjectId,
            timeRemaining: command.TimeRemaining,
            workLogs: null);

        await _repository.Add(task);
    }
}