﻿using System;
using Shared.Messaging.InProc.Queries;

namespace TimeTrackerCleanArch.Application.Project.GetProject;

public class GetProject : IQuery<ProjectResponse>
{
    public Guid Id { get; private set; }

    public GetProject(Guid id)
    {
        Id = id;
    }
}