﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Queries;
using TimeTrackerCleanArch.Application.Project.GetProject.Extensions;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Domain.Project;

namespace TimeTrackerCleanArch.Application.Project.GetProject;

internal class GetProjectQueryHandler : IQueryHandler<GetProject, ProjectResponse>
{
    private readonly IRepository<ProjectAggregate> _repository;

    public GetProjectQueryHandler(IRepository<ProjectAggregate> repository)
    {
        _repository = repository;
    }

    public async Task<ProjectResponse> HandleAsync(GetProject query)
    {
        var project = await _repository.Get(query.Id);

        return project.ToResponse();
    }
}