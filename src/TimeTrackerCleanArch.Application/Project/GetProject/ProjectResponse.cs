﻿using System;

namespace TimeTrackerCleanArch.Application.Project.GetProject
{
    public class ProjectResponse
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool IsActive { get; private set; }

        public ProjectResponse(Guid id, string name, string description, bool isActive)
        {
            Id = id;
            Name = name;
            Description = description;
            IsActive = isActive;
        }
    }

    namespace Extensions
    {
        public static class ProjectExtensions
        {
            public static ProjectResponse ToResponse(this Domain.Project.ProjectAggregate aggregate)
            {
                return new ProjectResponse(
                    id: aggregate.Id,
                    name: aggregate.Name,
                    description: aggregate.Description,
                    isActive: aggregate.IsActive);
            }
        }
    }
}
