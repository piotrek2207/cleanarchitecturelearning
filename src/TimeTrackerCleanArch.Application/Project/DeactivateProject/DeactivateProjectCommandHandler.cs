﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Commands;
using TimeTrackerCleanArch.Application.Exceptions;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Domain.Project;

namespace TimeTrackerCleanArch.Application.Project.DeactivateProject;

internal class DeactivateProjectCommandHandler : ICommandHandler<DeactivateProject>
{
    private readonly IRepository<ProjectAggregate> _repository;

    public DeactivateProjectCommandHandler(IRepository<ProjectAggregate> repository)
    {
        _repository = repository;
    }

    public async Task HandleAsync(DeactivateProject command)
    {
        var project = await _repository.Get(command.Id);
        if (project == null)
        {
            throw EntityNotFoundException<ProjectAggregate>.Create(command.Id);
        }

        project.Deactivate();

        await _repository.Update(project);
    }
}