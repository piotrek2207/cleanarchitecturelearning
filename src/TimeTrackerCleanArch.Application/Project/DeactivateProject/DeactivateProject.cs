﻿using System;
using Shared.Messaging.InProc.Commands;

namespace TimeTrackerCleanArch.Application.Project.DeactivateProject;

public class DeactivateProject : ICommand
{
    public Guid Id { get; }

    public DeactivateProject(Guid id)
    {
        Id = id;
    }
}