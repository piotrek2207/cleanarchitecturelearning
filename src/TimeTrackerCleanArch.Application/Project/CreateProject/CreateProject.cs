﻿using System;
using Shared.Messaging.Authorization.Attributes;
using Shared.Messaging.InProc.Commands;

namespace TimeTrackerCleanArch.Application.Project.CreateProject;

[AllowAnonymousAccess]
public class CreateProject : ICommand
{
    public Guid Id { get; private set; }
    public string Name { get; private set; }
    public string Description { get; private set; }

    public CreateProject(Guid id, string name, string description)
    {
        Id = id;
        Name = name;
        Description = description;
    }
}