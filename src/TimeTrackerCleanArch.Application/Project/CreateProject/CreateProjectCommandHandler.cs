﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Commands;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Domain.Project;

namespace TimeTrackerCleanArch.Application.Project.CreateProject;

internal class CreateProjectCommandHandler : ICommandHandler<CreateProject>
{
    private readonly IRepository<ProjectAggregate> _repository;

    public CreateProjectCommandHandler(IRepository<ProjectAggregate> repository)
    {
        _repository = repository;
    }

    public async Task HandleAsync(CreateProject command)
    {
        var project = new ProjectAggregate(
            id: command.Id,
            name: command.Name,
            description: command.Description);

        await _repository.Add(project);
    }
}