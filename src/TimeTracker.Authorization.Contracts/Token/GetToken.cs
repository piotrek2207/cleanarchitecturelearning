﻿using Shared.Messaging.Authorization.Attributes;
using Shared.Messaging.InProc.Queries;

namespace TimeTracker.Authorization.Contracts.Token;

[AllowAnonymousAccess]
public record GetToken(
        string UserName,
        string Password)
    : IQuery<TokenResponse>;