﻿namespace TimeTracker.Authorization.Contracts.Token;

public record TokenResponse(
    string access_token,
    int expires_at);