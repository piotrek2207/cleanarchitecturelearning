﻿using Shared.Domain.Common;

namespace TimeTracker.Authorization.Contracts.User
{
    public class UserRegistered : AggregateEvent
    {
        public UserRegistered(Guid aggregateId) : base(aggregateId)
        {
        }
    }
}
