﻿using Microsoft.AspNetCore.Identity;

namespace TimeTracker.Authorization.Entities;

internal class TimeTrackerUser : IdentityUser
{
    public TimeTrackerUser(string userName) : base(userName)
    {
    }
}