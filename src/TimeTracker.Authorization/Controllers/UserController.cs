﻿using Microsoft.AspNetCore.Mvc;
using Shared.Messaging.InProc.Commands;
using Shared.Messaging.InProc.Queries;
using TimeTracker.Authorization.Application.Commands.RegisterUser;
using TimeTracker.Authorization.Contracts.Token;

namespace TimeTracker.Authorization.Controllers;

public abstract class UserControllerBase : ControllerBase
{
    private readonly ICommandDispatcher _dispatcher;
    private readonly IQueryDispatcher _queryDispatcher;

    protected UserControllerBase(ICommandDispatcher dispatcher, IQueryDispatcher queryDispatcher)
    {
        _dispatcher = dispatcher;
        _queryDispatcher = queryDispatcher;
    }

    [HttpPost("token")]
    public async Task<TokenResponse> Login([FromBody] GetToken request)
    {
        return await _queryDispatcher.DispatchAsync(request);
    }

    [HttpPost("register-system-user")]
    public async Task<IActionResult> Register([FromBody] RegisterSystemUserCommand request)
    {
        request = request with { Id = request.Id == Guid.Empty ? Guid.NewGuid() : request.Id };
        await _dispatcher.DispatchAsync(request);
        return Ok();
    }
}