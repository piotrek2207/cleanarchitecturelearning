﻿using Shared.Messaging.Authorization.Attributes;
using Shared.Messaging.InProc.Commands;

namespace TimeTracker.Authorization.Application.Commands.RegisterUser;

[AllowAnonymousAccess]
public record RegisterSystemUserCommand(
        Guid Id,
        string UserName,
        string Password)
    : ICommand;