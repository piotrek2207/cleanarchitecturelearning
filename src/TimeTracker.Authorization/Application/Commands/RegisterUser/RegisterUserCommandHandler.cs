﻿using Shared.Domain.Common.Exceptions;
using Shared.Messaging.Events;
using Shared.Messaging.InProc.Commands;
using TimeTracker.Authorization.Common;
using TimeTracker.Authorization.Contracts.User;
using TimeTracker.Authorization.Entities;

namespace TimeTracker.Authorization.Application.Commands.RegisterUser;

internal class RegisterUserCommandHandler : 
    ICommandHandler<RegisterSystemUserCommand>
{
    private readonly TimeTrackerUserManager _userManager;
    private readonly IEventPublisher _eventPublisher;

    public RegisterUserCommandHandler(
        TimeTrackerUserManager userManager,
        IEventPublisher eventPublisher)
    {
        _userManager = userManager;
        _eventPublisher = eventPublisher;
    }

    public async Task HandleAsync(RegisterSystemUserCommand request)
    {
        await RegisterUser(request.Id, request.UserName, request.Password);
    }

    private async Task RegisterUser(Guid id, string userName, string password)
    {
        var existing = await _userManager.FindByNameAsync(userName);
        if (existing is not null)
        {
            throw new DomainException("Użytkownik już istnieje");
        }

        var userToCreate = new TimeTrackerUser(userName);
        userToCreate.Id = id.ToString();
        var result = await _userManager.CreateAsync(userToCreate);
        if (result.Succeeded)
        {
            var user = await _userManager.FindByNameAsync(userName);
            await _userManager.RemovePasswordAsync(user!);
            var passwordResult = await _userManager.AddPasswordAsync(user!, password);

            if (!passwordResult.Succeeded)
            {
                await _userManager.DeleteAsync(user);   
                throw new DomainException("Błąd podczas nadawania hasła użytkownikowi"
                                          + string.Join(",", passwordResult.Errors.Select(x => x.Description)));
            }

            await _eventPublisher.PublishAsync(new UserRegistered(Guid.Parse(user.Id)));
            return;
        }

        throw new DomainException("Tworzenie użytkownika nie powiodło się: " +
                                  string.Join(",", result.Errors.Select(x => x.Description)));
    }
}