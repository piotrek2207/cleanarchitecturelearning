﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Shared.Domain.Common.Exceptions;
using Shared.Messaging.Authorization;
using Shared.Messaging.InProc.Queries;
using TimeTracker.Authorization.Common;
using TimeTracker.Authorization.Contracts.Token;

namespace TimeTracker.Authorization.Application.Queries.Token;

internal class GetTokenHandler : IQueryHandler<GetToken, TokenResponse>
{
    private readonly TimeTrackerUserManager _userManager;
    private readonly IConfiguration _configuration;

    public GetTokenHandler(
        TimeTrackerUserManager userManager,
        IConfiguration configuration)
    {
        _userManager = userManager;
        _configuration = configuration;
    }

    public async Task<TokenResponse> HandleAsync(GetToken request)
    {
        var user = await _userManager.FindByNameAsync(request.UserName);

        if (user != null && await _userManager.CheckPasswordAsync(user, request.Password))
        {
            var userRoles = await _userManager.GetRolesAsync(user);

            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(CustomUserClaimTypes.UserId, user.Id)
            };

            foreach (var userRole in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, userRole));
            }

            var expiresAt = TimeSpan.FromDays(24 * 31); // think of a better way for long living tokens
            var token = GetAccessToken(authClaims, expiresAt);
            return new TokenResponse(
                new JwtSecurityTokenHandler().WriteToken(token),
                (int)expiresAt.TotalSeconds);
        }

        throw new NotAuthorizedException("Podano nieprawidłowe dane logowania");
    }

    private JwtSecurityToken GetAccessToken(List<Claim> authClaims, TimeSpan expiresAt)
    {
        var secret = _configuration["JWT:Secret"];
        var issuer = _configuration["JWT:ValidIssuer"];
        var audience = _configuration["JWT:ValidAudience"];

        var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));

        var token = new JwtSecurityToken(
            issuer: issuer,
            audience: audience,
            expires: DateTime.UtcNow.Add(expiresAt),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        return token;
    }
}