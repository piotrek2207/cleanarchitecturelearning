﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TimeTracker.Authorization.Entities;

namespace TimeTracker.Authorization.Database;

internal class TimeTrackerIdentityContext : IdentityDbContext<TimeTrackerUser>
{
    public TimeTrackerIdentityContext(DbContextOptions<TimeTrackerIdentityContext> options) : base(options)
    {
    }
}