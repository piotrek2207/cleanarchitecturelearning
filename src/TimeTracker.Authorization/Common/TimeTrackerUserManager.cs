﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shared.Messaging.InProc.Commands;
using TimeTracker.Authorization.Database;
using TimeTracker.Authorization.Entities;

namespace TimeTracker.Authorization.Common;

internal class TimeTrackerUserManager : UserManager<TimeTrackerUser>
{
    public TimeTrackerUserManager(
        IUserStore<TimeTrackerUser> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<TimeTrackerUser> passwordHasher,
        IEnumerable<IUserValidator<TimeTrackerUser>> userValidators,
        IEnumerable<IPasswordValidator<TimeTrackerUser>> passwordValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        IServiceProvider services,
        TimeTrackerIdentityContext context,
        ILogger<UserManager<TimeTrackerUser>> logger,
        ICommandDispatcher dispatcher)
        : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
    {
    }
}