﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Shared.Messaging.Extensions;
using TimeTracker.Authorization.Application.Commands.RegisterUser;
using TimeTracker.Authorization.Common;
using TimeTracker.Authorization.Database;
using TimeTracker.Authorization.Entities;

namespace TimeTracker.Authorization.Bootstrap;

public static class AuthorizationBootstrap
{
    public static IServiceCollection AddTimeTrackerAuthorization(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        var connectionString = $"UserName=postgres;Password=postgrespw;Host=localhost;Port=5432;Database=postgres;";
        services.AddDbContext<TimeTrackerIdentityContext>(opts => opts.UseNpgsql(connectionString));
        services.AddScoped<TimeTrackerUserManager>();
        services.RegisterCommandHandlersFromAssemblyContaining<RegisterUserCommandHandler>();

        services
            .AddIdentityCore<TimeTrackerUser>(
                options =>
                {
                    options.SignIn.RequireConfirmedAccount = false;

                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 1;
                }
            )
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<TimeTrackerIdentityContext>();

        services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })

            // Adding Jwt Bearer
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = configuration["JWT:ValidAudience"],
                    ValidIssuer = configuration["JWT:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
                };
            });

        return services;
    }

}