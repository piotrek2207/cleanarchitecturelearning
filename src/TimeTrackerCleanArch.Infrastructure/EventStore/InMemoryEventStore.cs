﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Shared.CommonExtensions.AsyncLock;
using Shared.Domain.Common;
using TimeTrackerCleanArch.Application.Exceptions;

namespace TimeTrackerCleanArch.Infrastructure.EventStore;

internal class InMemoryEventStore : IEventStore
{
    private static readonly Dictionary<string, EventsStream> _store = new Dictionary<string, EventsStream>();
    private const string Apply = "Apply";
    private static readonly IAsyncLock _lock = new InMemoryAsyncLock();

    public async Task Store<TAggregate>(Guid aggregateId, ICollection<IAggregateEvent> aggregateEvents) where TAggregate : Aggregate
    {
        await _lock.AcquireFor(async () =>
        {
            foreach (var aggregateEvent in aggregateEvents)
            {
                await AppendStream<TAggregate>(aggregateId, aggregateEvent, aggregateEvent.Version);
            }
        });
    }

    private Task AppendStream<TStream>(Guid streamId, IEvent @event, long version) where TStream : Aggregate
    {
        var key = GetKey<TStream>(streamId);
        if (!_store.ContainsKey(key))
        {
            _store[key] = new EventsStream();
        }

        var stream = _store[key];
        if (stream.Version >= version)
        {
            throw EventStreamIncorrectVersionException.CreteFor<TStream>(streamId, version);
        }

        _store[key].AddEvent(@event, version);
        _store[key].SetVersion(version);

        return Task.CompletedTask;
    }

    public async Task<TStream> AggregateStream<TStream>(Guid streamId) where TStream : Aggregate
    {
        var key = GetKey<TStream>(streamId);
        if (!_store.ContainsKey(key))
        {
            return null;
        }

        var aggregate = (TStream)Activator.CreateInstance(typeof(TStream), true);

        var events = _store[key].Events;

        foreach (var (@event, version) in events)
        {
            aggregate.InvokeIfExists(Apply, @event);
            aggregate.SetIfExists(nameof(Aggregate.Version), version);
        }

        await Task.CompletedTask;
        return aggregate;
    }

    private static string GetKey<TStream>(Guid streamId) where TStream : Aggregate
    {
        return typeof(TStream).FullName + streamId;
    }

    private class EventsStream
    {
        public long Version { get; private set; }
        public List<(IEvent @event, long version)> Events { get; private set; }

        public EventsStream()
        {
            Events = new List<(IEvent @event, long version)>();
        }

        public void AddEvent(IEvent @event, long version)
        {
            Events.Add((@event, version));
        }

        public void SetVersion(long version)
        {
            Version = version;
        }
    }
}

public static class Extensions
{
    public static object InvokeIfExists<T>(this T item, string methodName, object param)
    {
        var method = item.GetType()
            .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
            .Where(m =>
            {
                var parameters = m.GetParameters();
                return
                    m.Name == methodName
                    && parameters.Length == 1
                    && parameters.Single().ParameterType == param.GetType();
            })
            .SingleOrDefault();

        return method?.Invoke(item, new[] { param });
    }

    public static void SetIfExists<T>(this T item, string propertyName, object value) where T : notnull
    {
        item.GetType()
            .GetProperty(propertyName)?
            .SetValue(item, value);
    }
}