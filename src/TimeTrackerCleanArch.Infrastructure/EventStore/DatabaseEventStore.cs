﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Shared.Domain.Common;
using TimeTrackerCleanArch.Application.Exceptions;
using TimeTrackerCleanArch.Infrastructure.Database;

namespace TimeTrackerCleanArch.Infrastructure.EventStore;

internal class DatabaseEventStore : IEventStore
{
    private readonly TimeTrackerContext _context;

    public DatabaseEventStore(TimeTrackerContext context)
    {
        _context = context;
    }

    public async Task<TStream> AggregateStream<TStream>(Guid streamId) where TStream : Aggregate
    {
        var events = await _context.Events
            .Join(_context.EventStreams, x => x.StreamId, x => x.Id,
                ((eventData, stream) => new { eventData, stream }))
            .Where(x => x.stream.Type == typeof(TStream).FullName && x.stream.Id == streamId)
            .Select(x => x.eventData)
            .OrderBy(x => x.Version)
            .ToListAsync();

        if (!events.Any())
            return null;

        var aggregate = (TStream)Activator.CreateInstance(typeof(TStream), true);

        foreach (var eventData in events)
        {
            var eventType = Type.GetType(eventData.EventType + ", TimeTrackerCleanArch.Domain");
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            var @event = JsonConvert.DeserializeObject(eventData.EventEnvelope, eventType, settings);
            aggregate.InvokeIfExists("Apply", @event);
            aggregate.SetIfExists(nameof(Aggregate.Version), eventData.Version);
        }

        await Task.CompletedTask;
        return aggregate;
    }

    public async Task Store<TAggregate>(Guid aggregateId, ICollection<IAggregateEvent> aggregateEvents) where TAggregate : Aggregate
    {
        foreach (var aggregateEvent in aggregateEvents)
        {
            await AppendStream<TAggregate>(aggregateId, aggregateEvent, aggregateEvent.Version);
        }
    }

    private async Task AppendStream<TStream>(Guid streamId, IEvent @event, long version) where TStream : Aggregate
    {
        var isAdded = await _context.Database.GetDbConnection().QueryFirstAsync<bool>(
            @"SELECT ""TimeTracker"".""AppendEvent""(@Id, @Event, @Type, @StreamId, @StreamType, @EventTimestamp, @Version)",
            new
            {
                Id = Guid.NewGuid(),
                Event = JsonConvert.SerializeObject(@event),
                Type = @event.GetType().FullName,
                StreamId = streamId,
                StreamType = typeof(TStream).FullName,
                EventTimestamp = DateTime.UtcNow,
                Version = version - 1
            });

        if (!isAdded)
            throw EventStreamIncorrectVersionException.CreteFor<TStream>(streamId, version);
    }
}