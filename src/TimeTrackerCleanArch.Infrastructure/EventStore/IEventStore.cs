﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Infrastructure.EventStore;

internal interface IEventStore
{
    Task<TStream> AggregateStream<TStream>(Guid streamId) where TStream : Aggregate;

    Task Store<TAggregate>(Guid aggregateId, ICollection<IAggregateEvent> aggregateEvents) where TAggregate : Aggregate;
}