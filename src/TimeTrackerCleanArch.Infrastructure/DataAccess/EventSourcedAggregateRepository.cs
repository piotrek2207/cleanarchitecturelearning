﻿using System;
using System.Threading.Tasks;
using Shared.Domain.Common;
using Shared.Messaging.Events;
using TimeTrackerCleanArch.Application.Exceptions;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Infrastructure.Database;
using TimeTrackerCleanArch.Infrastructure.EventStore;

namespace TimeTrackerCleanArch.Infrastructure.DataAccess;

internal class EventSourcedAggregateRepository<TAggregate> : IRepository<TAggregate>
    where TAggregate : Aggregate
{
    private readonly IEventStore _eventStore;
    private readonly IEventPublisher _eventPublisher;
    private readonly TimeTrackerContext _context;

    public EventSourcedAggregateRepository(
        IEventStore eventStore,
        IEventPublisher eventPublisher,
        TimeTrackerContext context)
    {
        _eventStore = eventStore;
        _eventPublisher = eventPublisher;
        _context = context;
    }

    public async Task<TAggregate> Get(Guid id)
    {
        return await _eventStore.AggregateStream<TAggregate>(id);
    }

    public async Task Add(TAggregate aggregate)
    {
        await StoreAndPublishUncommittedEvents(aggregate);
    }

    public async Task Update(TAggregate aggregate)
    {
        var existingEntity = await Get(aggregate.Id);
        if (existingEntity is null)
        {
            throw EntityNotFoundException<TAggregate>.Create(aggregate.Id);
        }

        await StoreAndPublishUncommittedEvents(aggregate);
    }

    private async Task StoreAndPublishUncommittedEvents(TAggregate aggregate)
    {
        await using var dbTransaction = await _context.Database.BeginTransactionAsync();
        var events = aggregate.DequeueAllUncommittedEvents();
        await _eventStore.Store<TAggregate>(aggregate.Id, events);

        foreach (var aggregateEvent in events)
        {
            await _eventPublisher.PublishAsync(aggregateEvent);
        }

        await _context.SaveChangesAsync();
        await dbTransaction.CommitAsync();
    }
}