﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeTrackerCleanArch.Infrastructure.Migrations
{
    public partial class AddExternalEventsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExternalEvents",
                schema: "TimeTracker",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CorrelationId = table.Column<Guid>(nullable: false),
                    EventEnvelope = table.Column<string>(nullable: true),
                    IsForwarded = table.Column<bool>(nullable: false),
                    PublishedAt = table.Column<DateTime>(nullable: false),
                    ForwardedAt = table.Column<DateTime>(nullable: true),
                    EventType = table.Column<string>(nullable: true),
                    AggregateId = table.Column<Guid>(nullable: true),
                    AggregateVersion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalEvents", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExternalEvents",
                schema: "TimeTracker");
        }
    }
}
