﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeTrackerCleanArch.Infrastructure.Migrations
{
    public partial class AppendEventFunction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"CREATE OR REPLACE FUNCTION ""TimeTracker"".""AppendEvent""(id uuid, data text, type text, stream_id uuid, stream_type text, event_timestamp timestamp with time zone, expected_stream_version bigint default null) RETURNS boolean
LANGUAGE plpgsql
AS $$
DECLARE
    stream_version int;
BEGIN
    -- get stream version
    SELECT
        ""Version"" INTO stream_version
    FROM ""TimeTracker"".""EventStreams"" as s
    WHERE
        s.""Id"" = stream_id FOR UPDATE;
    -- if stream doesn't exist - create new one with version 0
    IF stream_version IS NULL THEN
        stream_version := 0;
        INSERT INTO ""TimeTracker"".""EventStreams""
            (""Id"", ""Type"", ""Version"")
        VALUES
            (stream_id, stream_type, stream_version);
    END IF;
    -- check optimistic concurrency
    IF expected_stream_version IS NOT NULL AND stream_version != expected_stream_version THEN
        RETURN FALSE;
    END IF;
    -- increment event_version
    stream_version := stream_version + 1;
    -- append event
    INSERT INTO ""TimeTracker"".""Events""
        (""Id"", ""EventEnvelope"", ""StreamId"", ""EventType"", ""Version"", ""Timestamp"")
    VALUES
        (id, data, stream_id, type, stream_version, event_timestamp);
    -- update stream version
    UPDATE ""TimeTracker"".""EventStreams"" as s
        SET ""Version"" = stream_version
    WHERE
        s.""Id"" = stream_id;
    RETURN TRUE;
END;
$$;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION append_event");
        }
    }
}
