﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeTrackerCleanArch.Infrastructure.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "TimeTracker");

            migrationBuilder.CreateTable(
                name: "Events",
                schema: "TimeTracker",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StreamId = table.Column<Guid>(nullable: false),
                    Version = table.Column<long>(nullable: false),
                    EventType = table.Column<string>(nullable: true),
                    EventEnvelope = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventStreams",
                schema: "TimeTracker",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Version = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventStreams", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Events",
                schema: "TimeTracker");

            migrationBuilder.DropTable(
                name: "EventStreams",
                schema: "TimeTracker");
        }
    }
}
