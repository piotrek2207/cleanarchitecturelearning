﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Domain.Common;
using Shared.Messaging.Bootstrap;
using TimeTrackerCleanArch.Domain.Common;
using TimeTrackerCleanArch.Domain.Project;
using TimeTrackerCleanArch.Domain.Task;
using TimeTrackerCleanArch.Infrastructure.DataAccess;
using TimeTrackerCleanArch.Infrastructure.Database;
using TimeTrackerCleanArch.Infrastructure.EventStore;

namespace TimeTrackerCleanArch.Infrastructure.Bootstrap;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        //services.AddSingleton<IEventStore, InMemoryEventStore>();
        services.AddScoped<IEventStore, DatabaseEventStore>();
        services.RegisterCommonComponents<TimeTrackerContext>($"UserName=postgres;Password=postgrespw;Host=localhost;Port=5432;Database=postgres;");

        services.RegisterEventSourcedRepositoryFor<ProjectAggregate>();
        services.RegisterEventSourcedRepositoryFor<TaskAggregate>();

        return services;
    }

    private static IServiceCollection RegisterEventSourcedRepositoryFor<TEntity>(this IServiceCollection services) 
        where TEntity : Aggregate
    {
        services.AddScoped<IRepository<TEntity>, EventSourcedAggregateRepository<TEntity>>();
        return services;
    }
}