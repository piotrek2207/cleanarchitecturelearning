﻿using Microsoft.EntityFrameworkCore;
using Shared.Messaging.Database;
using TimeTrackerCleanArch.Infrastructure.Database.Entities;

namespace TimeTrackerCleanArch.Infrastructure.Database;

internal class TimeTrackerContext : DatabaseContextBase
{

    public DbSet<EventStream> EventStreams { get; set; }

    public DbSet<EventData> Events { get; set; }


    public TimeTrackerContext() : base()
    {

    }

    public TimeTrackerContext(DbContextOptions<TimeTrackerContext> options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.HasDefaultSchema("TimeTracker");
        modelBuilder.ApplyConfiguration(new EventDataEfConfiguration());
        modelBuilder.ApplyConfiguration(new EventStreamEfConfiguration());
    }
}