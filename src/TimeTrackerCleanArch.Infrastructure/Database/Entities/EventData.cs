﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TimeTrackerCleanArch.Infrastructure.Database.Entities;

internal class EventData
{
    public Guid Id { get; private set; }

    public Guid StreamId { get; private set; }

    public long Version { get; private set; }

    public string EventType { get; private set; }

    public string EventEnvelope { get; private set; }

    public DateTime Timestamp { get; private set; }

    public EventData(Guid id, Guid streamId, long version, string eventType, string eventEnvelope, DateTime timestamp)
    {
        Id = id;
        StreamId = streamId;
        Version = version;
        EventType = eventType;
        EventEnvelope = eventEnvelope;
        Timestamp = timestamp;
    }
}

internal class EventDataEfConfiguration : IEntityTypeConfiguration<EventData>
{
    public void Configure(EntityTypeBuilder<EventData> builder)
    {
        builder.HasKey(x => x.Id);
    }
}