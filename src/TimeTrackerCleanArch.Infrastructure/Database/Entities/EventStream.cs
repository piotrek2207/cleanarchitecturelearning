﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TimeTrackerCleanArch.Infrastructure.Database.Entities;

internal class EventStream
{
    public Guid Id { get; private set; }

    public string Type { get; private set; }

    public long Version { get; private set; }

    public EventStream(Guid id, string type, long version)
    {
        Id = id;
        Type = type;
        Version = version;
    }
}

internal class EventStreamEfConfiguration : IEntityTypeConfiguration<EventStream>
{
    public void Configure(EntityTypeBuilder<EventStream> builder)
    {
        builder.HasKey(x => x.Id);
    }
}