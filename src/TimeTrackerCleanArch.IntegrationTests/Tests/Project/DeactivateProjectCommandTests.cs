﻿using System.Threading.Tasks;
using FluentAssertions;
using Shared.Domain.Common.Exceptions;
using Shared.TestsInfrastructure.Generators;
using TimeTrackerCleanArch.Application.Exceptions;
using TimeTrackerCleanArch.Application.Project.CreateProject;
using TimeTrackerCleanArch.Application.Project.DeactivateProject;
using TimeTrackerCleanArch.Domain.Project;
using TimeTrackerCleanArch.IntegrationTests.Infrastructure;
using Xunit;

namespace TimeTrackerCleanArch.IntegrationTests.Tests.Project;

public class DeactivateProjectCommandTests : IntegrationTestBase
{
    public DeactivateProjectCommandTests(TimeTrackerFixture fixture) : base(fixture)
    {
    }

    [Fact]
    public async Task DeactivateProject_WhenActive_ShouldDeactivate()
    {
        // Arrange
        var projectId = IdGenerator.GenerateId();
        var createProject = new CreateProject(
            id: projectId,
            name: StringGenerator.GenerateWord(),
            description: StringGenerator.GenerateSentence());

        await TestScope.SendCommandAsync(createProject);

        var deactivateProject = new DeactivateProject(projectId);

        // Act
        await TestScope.SendCommandAsync(deactivateProject);

        // Assert
        var project = await TestScope.EventStore.AggregateStream<ProjectAggregate>(projectId);
        project.Id.Should().Be(projectId);
        project.Version.Should().Be(2);
        project.IsActive.Should().BeFalse();
    }

    [Fact]
    public async Task DeactivateProject_WhenInactive_ShouldThrowDomainException()
    {
        // Arrange
        var projectId = IdGenerator.GenerateId();
        var createProject = new CreateProject(
            id: projectId,
            name: StringGenerator.GenerateWord(),
            description: StringGenerator.GenerateSentence());

        await TestScope.SendCommandAsync(createProject);

        var deactivateProject = new DeactivateProject(projectId);
        await TestScope.SendCommandAsync(deactivateProject);

        var deactivateProjectAgain = new DeactivateProject(projectId);

        // Act
        var exception = await Record.ExceptionAsync(async () =>
        {
            await TestScope.SendCommandAsync(deactivateProjectAgain);
        });

        // Assert
        exception.Should().BeOfType<DomainException>();
    }

    [Fact]
    public async Task DeactivateProject_WhenNotExists_ShouldThrowNotFound()
    {
        // Arrange
        var projectId = IdGenerator.GenerateId();
        var deactivateProject = new DeactivateProject(projectId);

        // Act
        var exception = await Record.ExceptionAsync(async () =>
        {
            await TestScope.SendCommandAsync(deactivateProject);
        });

        // Assert
        exception.Should()
            .BeOfType<EntityNotFoundException<ProjectAggregate>>()
            .Which.EntityId.Should().Be(projectId.ToString());
    }
}