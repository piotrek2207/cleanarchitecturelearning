﻿using System.Threading.Tasks;
using FluentAssertions;
using Shared.TestsInfrastructure.Generators;
using TimeTrackerCleanArch.Application.Exceptions;
using TimeTrackerCleanArch.Application.Project.CreateProject;
using TimeTrackerCleanArch.Domain.Project;
using TimeTrackerCleanArch.IntegrationTests.Infrastructure;
using Xunit;

namespace TimeTrackerCleanArch.IntegrationTests.Tests.Project;

public class CreateProjectCommandTests : IntegrationTestBase
{
    public CreateProjectCommandTests(TimeTrackerFixture fixture) : base(fixture)
    {
    }

    [Fact]
    public async Task CreateProject_WhenDoesntExist_ShouldCreateProject()
    {
        // Arrange
        var id = IdGenerator.GenerateId();
        var name = StringGenerator.GenerateWord();
        var description = StringGenerator.GenerateSentence();

        var createProject = new CreateProject(id, name, description);

        // Act
        await TestScope.SendCommandAsync(createProject);

        // Assert
        var project = await TestScope.EventStore.AggregateStream<ProjectAggregate>(id);
            
        project.Should().NotBeNull();
        project.Id.Should().Be(id);
        project.Version.Should().Be(1);
        project.IsActive.Should().BeTrue();
        project.Name.Should().Be(name);
        project.Description.Should().Be(description);
    }

    [Fact]
    public async Task CreateProject_WhenAlreadyExists_ShouldThrow()
    {
        // Arrange
        var id = IdGenerator.GenerateId();

        var createProject = new CreateProject(
            id: id, 
            name: StringGenerator.GenerateWord(),
            description: StringGenerator.GenerateSentence());

        var createProjectWithSameId = new CreateProject(
            id: id,
            name: StringGenerator.GenerateWord(),
            description: StringGenerator.GenerateSentence());

        // Act
        await TestScope.SendCommandAsync(createProject);

        var exception = await Record.ExceptionAsync(async () =>
        {
            await TestScope.SendCommandAsync(createProjectWithSameId);
        });

        // Assert
        exception.Should().BeOfType<EventStreamIncorrectVersionException>();
    }
}