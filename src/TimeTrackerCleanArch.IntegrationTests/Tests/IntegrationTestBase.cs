﻿using TimeTrackerCleanArch.IntegrationTests.Infrastructure;

namespace TimeTrackerCleanArch.IntegrationTests.Tests;

public class IntegrationTestBase
{
    protected TimeTrackerTestScope TestScope { get; }

    public IntegrationTestBase(TimeTrackerFixture fixture)
    {
        TestScope = fixture.CreateTestScope();
    }
}