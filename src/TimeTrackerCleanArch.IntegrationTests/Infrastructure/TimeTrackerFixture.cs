﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Bootstrap;
using TimeTrackerCleanArch.Application.Bootstrap;
using TimeTrackerCleanArch.Infrastructure.Bootstrap;
using TimeTrackerCleanArch.Infrastructure.Database;
using TimeTrackerCleanArch.Tests.Common.Extensions;

namespace TimeTrackerCleanArch.IntegrationTests.Infrastructure;

public class BaseDatabaseFixture : IDisposable
{
    public IServiceProvider Services { get; }

    public BaseDatabaseFixture()
    {
        Services = ConfigureServices();
        var context = Services.GetRequiredService<TimeTrackerContext>();
        context.Database.EnsureDeleted();
        context.Database.Migrate();
    }

    public void Dispose()
    {
        var context = Services.GetRequiredService<TimeTrackerContext>();
        context.Database.EnsureDeleted();
    }

    private IServiceProvider ConfigureServices()
    {
        var services = new ServiceCollection();

        services.AddCQRS().InMemory();
        services.AddInfrastructure();
        services.AddApplication();
        OverrideDatabaseContext(services);

        return services.BuildServiceProvider();
    }

    private void OverrideDatabaseContext(ServiceCollection services)
    {
        var databaseName = $"TimeTrackerTestDatabase-{DateTime.UtcNow:yyyyMMddHHmmss}";

        services.RemoveImplementationsOfType<DbContextOptions<TimeTrackerContext>>();
        services.AddDbContext<TimeTrackerContext>(opts =>
        {
            opts.UseNpgsql($"UserName=postgres;Password=postgrespw;Host=localhost;Port=5432;Database={databaseName};");
        });
    }
}

public class TimeTrackerFixture : BaseDatabaseFixture
{
    public TimeTrackerTestScope CreateTestScope()
    {
        return new TimeTrackerTestScope(Services);
    }
}