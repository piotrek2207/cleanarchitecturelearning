﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging;
using Shared.Messaging.InProc.Commands;
using TimeTrackerCleanArch.Infrastructure.EventStore;

namespace TimeTrackerCleanArch.IntegrationTests.Infrastructure;

public class TimeTrackerTestScope
{
    public IServiceProvider ServiceProvider { get; private set; }
    internal IEventStore EventStore => ServiceProvider.GetRequiredService<IEventStore>();

    public TimeTrackerTestScope(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
    }

    public async Task SendCommandAsync<TCommand>(TCommand command) where TCommand : ICommand
    {
        var serviceScopeFactory = ServiceProvider.GetRequiredService<IServiceScopeFactory>();
        using var serviceScope = serviceScopeFactory.CreateScope();
        var callContext = serviceScope.ServiceProvider.GetRequiredService<CallContext>();
        callContext.SetCorrelationId(Guid.NewGuid());

        var commandDispatcherFactory = serviceScope.ServiceProvider.GetRequiredService<ICommandDispatcherFactory>();
        var commandDispatcher = commandDispatcherFactory.CreateFor(callContext);

        await commandDispatcher.DispatchAsync(command);
    }
}