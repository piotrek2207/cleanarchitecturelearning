﻿using Microsoft.Extensions.DependencyInjection;
using TimeTrackerCleanArch.IntegrationTests.Infrastructure;

namespace TimeTrackerCleanArch.IntegrationTests;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<TimeTrackerFixture>();
    }
}