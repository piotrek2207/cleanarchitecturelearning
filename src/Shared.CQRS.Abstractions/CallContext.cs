﻿using System;
using Newtonsoft.Json;

namespace Shared.Messaging;

public class CallContext
{
    public Guid CorrelationId { get; private set; }
    public Guid? ProjectId { get; private set; }
    public Guid? UserId { get; private set; }

    [JsonConstructor]
    private CallContext(Guid correlationId, Guid? projectId, Guid? userId)
    {
        CorrelationId = correlationId;
        ProjectId = projectId;
        UserId = userId;
    }

    public static CallContext CreateNew()
    {
        return new CallContext(Guid.Empty, null, null);
    }

    public void SetCorrelationId(Guid correlationId)
    {
        CorrelationId = correlationId;
    }

    public void SetProjectId(Guid projectId)
    {
        ProjectId = projectId;
    }

    public void SetUserId(Guid userId)
    {
        UserId = userId;
    }

    public void SubstituteWith(CallContext callContext)
    {
        CorrelationId = callContext.CorrelationId;
        ProjectId = callContext.ProjectId;
        UserId = callContext.UserId;
    }
}