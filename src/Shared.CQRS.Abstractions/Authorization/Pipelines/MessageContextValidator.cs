﻿using System.Reflection;
using Shared.Messaging.Authorization.Attributes;
using Shared.Messaging.Exceptions;

namespace Shared.Messaging.Authorization.Pipelines;

internal static class MessageContextValidator
{
    public static void ValidateMessage<TMessage>(TMessage command, CallContext callContext)
    {
        var allowAnonymousAccess = command.GetType().GetCustomAttribute<AllowAnonymousAccess>();
        if (allowAnonymousAccess == null && !callContext.UserId.HasValue)
        {
            throw InvalidContextException.UserContextMissing();
        }

        var requireClinicContext = command.GetType().GetCustomAttribute<RequireProjectContextAttribute>();
        if (requireClinicContext != null && !callContext.ProjectId.HasValue)
        {
            throw InvalidContextException.ProjectContextMissing();
        }
    }
}