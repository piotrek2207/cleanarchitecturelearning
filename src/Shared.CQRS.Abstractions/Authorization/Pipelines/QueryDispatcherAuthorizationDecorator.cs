﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Queries;

namespace Shared.Messaging.Authorization.Pipelines;

internal class QueryDispatcherAuthorizationDecorator : IQueryDispatcher
{
    private readonly IQueryDispatcher _decorated;
    private readonly CallContext _callContext;

    public QueryDispatcherAuthorizationDecorator(
        IQueryDispatcher decorated,
        CallContext callContext)
    {
        _decorated = decorated;
        _callContext = callContext;
    }

    public async Task<TResponse> DispatchAsync<TResponse>(IQuery<TResponse> query)
    {
        MessageContextValidator.ValidateMessage(query, _callContext);

        return await _decorated.DispatchAsync(query);
    }
}