﻿using System.Threading.Tasks;
using Shared.Messaging.InProc.Commands;

namespace Shared.Messaging.Authorization.Pipelines
{
    internal class CommandDispatcherAuthorizationDecorator : ICommandDispatcher
    {
        private readonly ICommandDispatcher _decorated;
        private readonly CallContext _callContext;

        public CommandDispatcherAuthorizationDecorator(
            ICommandDispatcher decorated,
            CallContext callContext)
        {
            _decorated = decorated;
            _callContext = callContext;
        }

        public async Task DispatchAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            MessageContextValidator.ValidateMessage(command, _callContext);

            await _decorated.DispatchAsync(command);
        }
    }
}
