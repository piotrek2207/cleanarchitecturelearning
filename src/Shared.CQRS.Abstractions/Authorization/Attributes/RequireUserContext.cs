﻿using System;

namespace Shared.Messaging.Authorization.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class RequireUserContextAttribute : Attribute
{
}