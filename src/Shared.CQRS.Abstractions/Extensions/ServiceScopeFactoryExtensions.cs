﻿using Microsoft.Extensions.DependencyInjection;

namespace Shared.Messaging.Extensions;

public static class ServiceScopeFactoryExtensions
{
    public static IServiceScope CreateScopeWithCallContext(
        this IServiceScopeFactory scopeFactory,
        CallContext callContext)
    {
        var newScope = scopeFactory.CreateScope();
        var newCallContext = newScope.ServiceProvider.GetService<CallContext>();
        newCallContext.SubstituteWith(callContext);

        return newScope;
    }
}