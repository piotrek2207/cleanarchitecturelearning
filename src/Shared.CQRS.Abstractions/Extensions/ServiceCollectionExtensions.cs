﻿using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.InProc.Commands;
using Shared.Messaging.InProc.Queries;

namespace Shared.Messaging.Extensions;

public static class ServiceCollectionExtensions
{
    public static void RegisterCommandHandlersFromAssemblyContaining<T>(this IServiceCollection services)
    {
        services.Scan(scan => scan
            .FromAssemblyOf<T>()
            .AddClasses(classes => classes.AssignableTo(typeof(ICommandHandler<>)))
            .AsImplementedInterfaces()
            .WithScopedLifetime());
    }

    public static void RegisterQueryHandlersFromAssemblyContaining<T>(this IServiceCollection services)
    {
        services.Scan(scan => scan
            .FromAssemblyOf<T>()
            .AddClasses(classes => classes.AssignableTo(typeof(IQueryHandler<,>)))
            .AsImplementedInterfaces()
            .WithScopedLifetime());
    }
}