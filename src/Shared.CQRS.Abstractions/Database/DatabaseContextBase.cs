﻿using Microsoft.EntityFrameworkCore;
using Shared.Messaging.Events;

namespace Shared.Messaging.Database
{
    public abstract class DatabaseContextBase : DbContext
    {
        internal DbSet<EventRow> ExternalEvents { get; set; }

        protected DatabaseContextBase()
        {
        }

        protected DatabaseContextBase(DbContextOptions options) : base(options)
        {
        }
    }
}
