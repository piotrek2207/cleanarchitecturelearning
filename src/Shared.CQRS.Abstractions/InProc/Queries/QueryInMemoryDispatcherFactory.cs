﻿using Microsoft.Extensions.DependencyInjection;

namespace Shared.Messaging.InProc.Queries;

public class QueryInMemoryDispatcherFactory : IQueryDispatcherFactory
{
    private readonly IServiceScopeFactory _scopeFactory;

    public QueryInMemoryDispatcherFactory(IServiceScopeFactory scopeFactory)
    {
        _scopeFactory = scopeFactory;
    }

    public IQueryDispatcher CreateFor(CallContext callContext)
    {
        return new QueryInMemoryDispatcher(callContext, _scopeFactory);
    }
}