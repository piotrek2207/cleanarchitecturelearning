﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Extensions;

namespace Shared.Messaging.InProc.Queries;

internal class QueryInMemoryDispatcher : IQueryDispatcher
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly CallContext _callContext;

    public QueryInMemoryDispatcher(CallContext callContext, IServiceScopeFactory scopeFactory)
    {
        _callContext = callContext;
        _scopeFactory = scopeFactory;
    }

    public async Task<TResponse> DispatchAsync<TResponse>(IQuery<TResponse> query)
    {
        using var scope = _scopeFactory.CreateScopeWithCallContext(_callContext);

        var queryType = query.GetType();
        var responseType = typeof(TResponse);
        var queryHandlerType = typeof(IQueryHandler<,>).MakeGenericType(queryType, responseType);

        var handler = scope.ServiceProvider.GetService(queryHandlerType);
        var handlerWrapperType = typeof(QueryHandlerWrapper<,>).MakeGenericType(queryType, responseType);
        var handlerWrapper = (IQueryHandlerWrapper<TResponse>) Activator.CreateInstance(
            handlerWrapperType, handler);

        return await handlerWrapper.HandleAsync(query);
    }

    private interface IQueryHandlerWrapper<TResponse>
    {
        Task<TResponse> HandleAsync(object query);
    }

    private class QueryHandlerWrapper<TQuery, TResponse> : IQueryHandlerWrapper<TResponse>
        where TQuery : IQuery<TResponse>
    {
        private readonly IQueryHandler<TQuery, TResponse> _handler;

        public QueryHandlerWrapper(object handler)
        {
            _handler = (IQueryHandler<TQuery, TResponse>)handler;
        }

        public async Task<TResponse> HandleAsync(object query)
        {
            return await _handler.HandleAsync((TQuery)query);
        }
    }
}