﻿namespace Shared.Messaging.InProc.Queries;

public interface IQueryDispatcherFactory
{
    IQueryDispatcher CreateFor(CallContext callContext);
}