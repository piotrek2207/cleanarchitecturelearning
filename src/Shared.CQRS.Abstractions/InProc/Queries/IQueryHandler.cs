﻿using System.Threading.Tasks;

namespace Shared.Messaging.InProc.Queries;

public interface IQueryHandler<TQuery, TResponse>
    where TQuery : IQuery<TResponse>
{
    Task<TResponse> HandleAsync(TQuery query);
}