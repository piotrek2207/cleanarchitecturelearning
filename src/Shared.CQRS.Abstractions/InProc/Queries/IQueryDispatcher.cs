﻿using System.Threading.Tasks;

namespace Shared.Messaging.InProc.Queries;

public interface IQueryDispatcher
{
    Task<TResponse> DispatchAsync<TResponse>(IQuery<TResponse> query);
}