﻿using System.Threading.Tasks;

namespace Shared.Messaging.InProc.Commands;

public interface ICommandHandler<TCommand> where TCommand : ICommand
{
    Task HandleAsync(TCommand command);
}