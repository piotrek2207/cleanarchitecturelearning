﻿namespace Shared.Messaging.InProc.Commands;

public interface ICommandDispatcherFactory
{
    ICommandDispatcher CreateFor(CallContext callContext);
}