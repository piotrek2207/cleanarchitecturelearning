﻿using System.Threading.Tasks;

namespace Shared.Messaging.InProc.Commands;

public interface ICommandDispatcher
{
    Task DispatchAsync<TCommand>(TCommand command) where TCommand : ICommand;
}