﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Extensions;

namespace Shared.Messaging.InProc.Commands;

public class CommandInMemoryDispatcher : ICommandDispatcher
{
    private readonly CallContext _callContext;
    private readonly IServiceScopeFactory _scopeFactory;

    public CommandInMemoryDispatcher(CallContext callContext, IServiceScopeFactory scopeFactory)
    {
        _callContext = callContext;
        _scopeFactory = scopeFactory;
    }

    public async Task DispatchAsync<TCommand>(TCommand command) where TCommand : ICommand
    {
        using var scope = _scopeFactory.CreateScopeWithCallContext(_callContext);

        var commandHandlerType = typeof(ICommandHandler<TCommand>);
        var handler = (ICommandHandler<TCommand>) scope.ServiceProvider.GetService(commandHandlerType);

        await handler.HandleAsync(command);
    }
}