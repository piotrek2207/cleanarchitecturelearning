﻿using Microsoft.Extensions.DependencyInjection;

namespace Shared.Messaging.InProc.Commands;

public class CommandInMemoryDispatcherFactory : ICommandDispatcherFactory
{
    private readonly IServiceScopeFactory _scopeFactory;

    public CommandInMemoryDispatcherFactory(IServiceScopeFactory scopeFactory)
    {
        _scopeFactory = scopeFactory;
    }

    public ICommandDispatcher CreateFor(CallContext callContext)
    {
        return new CommandInMemoryDispatcher(callContext, _scopeFactory);
    }
}