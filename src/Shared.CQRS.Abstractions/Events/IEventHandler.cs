﻿using MediatR;
using Shared.Domain.Common;

namespace Shared.Messaging.Events
{
    public interface IEventHandler<TEvent> : INotificationHandler<TEvent>
        where TEvent : IEvent
    {
    }
}
