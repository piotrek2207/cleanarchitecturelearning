﻿using System;
using Shared.Domain.Common;

namespace Shared.Messaging.Events;

internal class EventEnvelope
{
    public DateTime Timestamp { get; private set; }

    public CallContext CallContext { get; private set; }

    public IEvent EventData { get; private set; }

    public EventEnvelope(DateTime timestamp, CallContext callContext, IEvent eventData)
    {
        Timestamp = timestamp;
        CallContext = callContext;
        EventData = eventData;
    }
}

internal static class EventExtensions
{
    internal static EventEnvelope WrapInEnvelope(this IEvent @event, CallContext callContext, DateTime timestamp)
    {
        return new EventEnvelope(
            timestamp,
            callContext,
            @event);
    }
}