﻿using System;
using System.Threading.Tasks;
using Shared.Domain.Common;

namespace Shared.Messaging.Events;

public interface IEventPublisher
{
    Task PublishAsync(IEvent @event);
}

internal class EventPublisher : IEventPublisher
{
    private readonly IExternalEventsAppender _eventsAppender;
    private readonly CallContext _callContext;

    public EventPublisher(IExternalEventsAppender eventsAppender, CallContext callContext)
    {
        _eventsAppender = eventsAppender;
        _callContext = callContext;
    }

    public async Task PublishAsync(IEvent @event)
    {
        await _eventsAppender.AppendAsync(@event.WrapInEnvelope(_callContext, DateTime.UtcNow));
    }
}