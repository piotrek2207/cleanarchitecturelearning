﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Shared.Domain.Common;
using Shared.Messaging.Database;
using Shared.Messaging.Extensions;

namespace Shared.Messaging.Events
{
    internal class EventForwarderHostedService : BackgroundService
    {
        private readonly IEnumerable<IEventForwarder> _forwarder;

        public EventForwarderHostedService(IEnumerable<IEventForwarder> forwarder)
        {
            _forwarder = forwarder;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                foreach (var forwarder in _forwarder)
                {
                    await forwarder.PublishPendingEvents(10);
                }

                await Task.Delay(TimeSpan.FromMilliseconds(100));
            }
        }
    }

    internal interface IEventForwarder
    {
        Task PublishPendingEvents(int batch);
    }

    internal class EventForwarder<TDbContext> : IEventForwarder where TDbContext : DatabaseContextBase
    {
        private readonly IServiceScopeFactory _factory;

        public EventForwarder(IServiceScopeFactory factory)
        {
            _factory = factory;
        }

        public async Task PublishPendingEvents(int batch)
        {
            using var scope = _factory.CreateScope();
           
            var context = scope.ServiceProvider.GetService<TDbContext>();
            var toPublish = await context.ExternalEvents
                .AsNoTracking()
                .Where(x => !x.IsForwarded)
                .OrderBy(x => x.PublishedAt)
                .Take(batch)
                .ToListAsync();

            foreach (var eventRow in toPublish)
            {
                var settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.All
                };

                var eventEnvelope = JsonConvert.DeserializeObject<EventEnvelope>(eventRow.EventEnvelope, settings);
                var callContext = eventEnvelope.CallContext;

                
                using var eventScope = _factory.CreateScopeWithCallContext(callContext);
                var dbContext = eventScope.ServiceProvider.GetService<TDbContext>();
                var mediator = eventScope.ServiceProvider.GetRequiredService<IMediator>();
                await mediator.Publish(eventEnvelope.EventData);
                eventRow.MarkAsForwarded();
                dbContext.Update(eventRow);
                await dbContext.SaveChangesAsync();

                //var eventType = eventEnvelope.EventData.GetType();
                //var handlerType = typeof(HandlerWrapper<>).MakeGenericType(eventType);

                //var eventHandlerWrapper = (IHandlerWrapper)Activator.CreateInstance(handlerType, eventScope.ServiceProvider);
                //await eventHandlerWrapper!.HandleAsync(eventEnvelope.EventData);
            }
        }

        private interface IHandlerWrapper
        {
            Task HandleAsync(object @event);
        }

        private class HandlerWrapper<TEvent> : IHandlerWrapper
            where TEvent : IEvent
        {
            private readonly IServiceProvider _serviceProvider;

            public HandlerWrapper(IServiceProvider serviceProvider)
            {
                _serviceProvider = serviceProvider;
            }

            public async Task HandleAsync(object @event)
            {
                var handlers = _serviceProvider.GetRequiredService<IEnumerable<IEventHandler<TEvent>>>();

                foreach (var eventHandler in handlers)
                {
                    await eventHandler.Handle((TEvent)@event, default);
                }
            }
        }
    }
}
