﻿using System;

namespace Shared.Messaging.Events;

internal class EventRow
{
    public Guid Id { get; private set; }

    public Guid CorrelationId { get; private set; }

    public string EventEnvelope { get; private set; }

    public bool IsForwarded { get; private set; }

    public DateTime PublishedAt { get; private set; }

    public DateTime? ForwardedAt { get; private set; }

    public string EventType { get; private set; }

    public Guid? AggregateId { get; private set; }

    public long? AggregateVersion { get; private set; }

    public EventRow(Guid id, Guid correlationId, string eventEnvelope, bool isForwarded, DateTime publishedAt, DateTime? forwardedAt, string eventType, Guid? aggregateId, long? aggregateVersion)
    {
        Id = id;
        CorrelationId = correlationId;
        EventEnvelope = eventEnvelope;
        IsForwarded = isForwarded;
        PublishedAt = publishedAt;
        ForwardedAt = forwardedAt;
        EventType = eventType;
        AggregateId = aggregateId;
        AggregateVersion = aggregateVersion;
    }

    public void MarkAsForwarded()
    {
        ForwardedAt = DateTime.UtcNow;
        IsForwarded = true;
    }
}