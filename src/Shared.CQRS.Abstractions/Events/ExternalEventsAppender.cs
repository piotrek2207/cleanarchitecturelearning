﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Domain.Common;
using Shared.Messaging.Database;

namespace Shared.Messaging.Events;

internal interface IExternalEventsAppender
{
    Task AppendAsync(EventEnvelope eventEnvelope);
}

internal class ExternalEventsAppender<TDbContext> : IExternalEventsAppender
    where TDbContext : DatabaseContextBase
{
    private readonly TDbContext _context;

    public ExternalEventsAppender(TDbContext context)
    {
        _context = context;
    }

    public async Task AppendAsync(EventEnvelope eventEnvelope)
    {
        var settings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        var aggregateEvent = eventEnvelope.EventData as IAggregateEvent;

        var eventRow = new EventRow(
            id: Guid.NewGuid(),
            correlationId: eventEnvelope.CallContext.CorrelationId,
            eventEnvelope: JsonConvert.SerializeObject(eventEnvelope, settings),
            isForwarded: false,
            publishedAt: DateTime.UtcNow,
            forwardedAt: null,
            eventType: eventEnvelope.EventData.GetType().FullName,
            aggregateId: aggregateEvent?.AggregateId,
            aggregateVersion: aggregateEvent?.Version);
        _context.ExternalEvents.Add(eventRow);
        await _context.SaveChangesAsync(); // TODO: make part of pipeline
        await Task.CompletedTask;
    }
}