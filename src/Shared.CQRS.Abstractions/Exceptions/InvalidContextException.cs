﻿using System;

namespace Shared.Messaging.Exceptions
{
    public class InvalidContextException : Exception
    {
        private InvalidContextException(string message) : base(message)
        {
        }

        public static InvalidContextException ProjectContextMissing()
            => new("Project Context is missing");

        public static InvalidContextException UserContextMissing()
            => new("User Context is required for this operation");
    }
}
