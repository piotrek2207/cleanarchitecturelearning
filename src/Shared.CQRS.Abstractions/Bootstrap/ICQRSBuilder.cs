﻿using Microsoft.Extensions.DependencyInjection;

namespace Shared.Messaging.Bootstrap;

public interface ICQRSBuilder
{
    IServiceCollection Services { get; }
    ICQRSBuilder WithDefaultPipelines();
}