﻿using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.InProc.Commands;
using Shared.Messaging.InProc.Queries;

namespace Shared.Messaging.Bootstrap;

public static class BuilderExtensions
{
    public static ICQRSBuilder InMemory(this ICQRSBuilder builder)
    {
        builder.Services.AddScoped<ICommandDispatcher, CommandInMemoryDispatcher>();
        builder.Services.AddSingleton<ICommandDispatcherFactory, CommandInMemoryDispatcherFactory>();

        builder.Services.AddScoped<IQueryDispatcher, QueryInMemoryDispatcher>();
        builder.Services.AddScoped<IQueryDispatcherFactory, QueryInMemoryDispatcherFactory>();

        return builder;
    }
}