﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Database;
using Shared.Messaging.Events;

namespace Shared.Messaging.Bootstrap;

public static class CQRSStartup
{
    public static ICQRSBuilder AddCQRS(this IServiceCollection services)
    {
        services.AddScoped(ctx => CallContext.CreateNew());

        return new CQRSBuilder(services);
    }

    public static IServiceCollection RegisterCommonComponents<TDbContext>(this IServiceCollection services, string connString)
        where TDbContext : DatabaseContextBase
    {
        services.AddDbContext<TDbContext>(opts => { opts.UseNpgsql(connString); });

        services.AddScoped<IExternalEventsAppender, ExternalEventsAppender<TDbContext>>();
        services.AddScoped<IEventPublisher, EventPublisher>();
        services.AddSingleton<IEventForwarder, EventForwarder<TDbContext>>();
        return services;
    }

    public static IServiceCollection RegisterEventForwarder(this IServiceCollection services)
    {
        services.AddHostedService<EventForwarderHostedService>();
        return services;
    }
}