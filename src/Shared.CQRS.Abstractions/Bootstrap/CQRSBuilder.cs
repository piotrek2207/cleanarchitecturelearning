﻿using Microsoft.Extensions.DependencyInjection;
using Shared.Messaging.Authorization.Pipelines;
using Shared.Messaging.InProc.Commands;
using Shared.Messaging.InProc.Queries;

namespace Shared.Messaging.Bootstrap;

internal class CQRSBuilder : ICQRSBuilder
{
    public IServiceCollection Services { get; }

    public CQRSBuilder(IServiceCollection serviceCollection)
    {
        Services = serviceCollection;
    }

    public ICQRSBuilder WithDefaultPipelines()
    {
        Services.Decorate<ICommandDispatcher, CommandDispatcherAuthorizationDecorator>();
        Services.Decorate<IQueryDispatcher, QueryDispatcherAuthorizationDecorator>();
        return this;
    }
}