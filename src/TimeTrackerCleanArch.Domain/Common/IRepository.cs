﻿using System;
using Tasks = System.Threading.Tasks;

namespace TimeTrackerCleanArch.Domain.Common;

public interface IRepository<TEntity>
{
    Tasks.Task<TEntity> Get(Guid id);
    Tasks.Task Add(TEntity aggregate);
    Tasks.Task Update(TEntity aggregate);
}