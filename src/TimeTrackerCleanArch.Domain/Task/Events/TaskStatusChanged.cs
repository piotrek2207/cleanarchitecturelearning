﻿using System;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Domain.Task.Events;

public class TaskStatusChanged : AggregateEvent
{
    public TaskStatusEnum PreviousStatus { get; private set; }
    public TaskStatusEnum NewStatus { get; private set; }

    public TaskStatusChanged(
        Guid aggregateId,
        TaskStatusEnum previousStatus,
        TaskStatusEnum newStatus)
        : base(aggregateId)
    {
        PreviousStatus = previousStatus;
        NewStatus = newStatus;
    }
}