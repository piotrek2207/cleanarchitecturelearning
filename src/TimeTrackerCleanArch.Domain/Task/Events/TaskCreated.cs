﻿using System;
using System.Collections.Generic;
using Shared.Domain.Common;
using TimeTrackerCleanArch.Domain.Task.Events.Dto;

namespace TimeTrackerCleanArch.Domain.Task.Events;

public class TaskCreated : AggregateEvent
{
    public string Title { get; }
    public string Description { get; }
    public TaskStatusEnum Status { get; }
    public Guid ProjectId { get; }
    public TimeSpan TimeRemaining { get; }
    public IReadOnlyCollection<TaskWorkLogDetails> WorkLogs { get; }

    public TaskCreated(
        Guid aggregateId,
        string title,
        string description,
        TaskStatusEnum status,
        Guid projectId, 
        TimeSpan timeRemaining,
        IReadOnlyCollection<TaskWorkLogDetails> workLogs)
        : base(aggregateId)
    {
        Title = title;
        Description = description;
        Status = status;
        ProjectId = projectId;
        TimeRemaining = timeRemaining;
        WorkLogs = workLogs;
    }
}