﻿using System;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Domain.Task.Events;

public class TaskSummaryChanged : AggregateEvent
{
    public string Title { get; }
    public string Description { get; }

    public TaskSummaryChanged(
        Guid aggregateId, 
        string title, 
        string description) 
        : base(aggregateId)
    {
        Title = title;
        Description = description;
    }
}