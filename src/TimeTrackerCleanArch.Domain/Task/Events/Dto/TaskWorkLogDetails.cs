﻿using System;
using DomainWorkLog = TimeTrackerCleanArch.Domain.Task.TaskWorkLog;

namespace TimeTrackerCleanArch.Domain.Task.Events.Dto
{
    public class TaskWorkLogDetails
    {
        public Guid Id { get; private set; }
        public TimeSpan TimeSpent { get; private set; }
        public DateTime Date { get; private set; }

        public TaskWorkLogDetails(Guid id, DateTime date, TimeSpan timeSpent)
        {
            Id = id;
            Date = date;
            TimeSpent = timeSpent;
        }
    }

    namespace Extensions
    {
        public static class TaskWorkLogDetailsExtensions
        {
            public static DomainWorkLog ToDomainEntity(this TaskWorkLogDetails details)
            {
                return new TaskWorkLog(
                    details.Id,
                    details.Date,
                    details.TimeSpent);
            }
        }
    }
}
