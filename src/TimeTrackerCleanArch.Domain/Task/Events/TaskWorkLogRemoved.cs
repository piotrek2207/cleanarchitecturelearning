﻿using System;
using Shared.Domain.Common;
using TimeTrackerCleanArch.Domain.Task.Events.Dto;

namespace TimeTrackerCleanArch.Domain.Task.Events;

public class TaskWorkLogRemoved : AggregateEvent
{
    public TaskWorkLogDetails WorkLogDetails { get; private set; }

    public TaskWorkLogRemoved(
        Guid aggregateId, 
        TaskWorkLogDetails workLogDetails) 
        : base(aggregateId)
    {
        WorkLogDetails = workLogDetails;
    }
}