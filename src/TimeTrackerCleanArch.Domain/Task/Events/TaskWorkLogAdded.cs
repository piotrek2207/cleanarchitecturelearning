﻿using System;
using Shared.Domain.Common;
using TimeTrackerCleanArch.Domain.Task.Events.Dto;

namespace TimeTrackerCleanArch.Domain.Task.Events;

public class TaskWorkLogAdded : AggregateEvent
{
    public TaskWorkLogDetails WorkLogDetails { get; private set; }

    public TaskWorkLogAdded(
        Guid aggregateId,
        TaskWorkLogDetails workLogDetails) 
        : base(aggregateId)
    {
        WorkLogDetails = workLogDetails;
    }
}