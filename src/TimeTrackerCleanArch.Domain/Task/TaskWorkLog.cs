﻿using System;

namespace TimeTrackerCleanArch.Domain.Task;

public class TaskWorkLog
{
    public Guid Id { get; private set; }
    public DateTime Date { get; private set; }
    public TimeSpan TimeSpent { get; private set; }

    public TaskWorkLog(Guid id, DateTime date, TimeSpan timeSpent)
    {
        Id = id;
        Date = date;
        TimeSpent = timeSpent;
    }

    public void Update(DateTime date, TimeSpan timeSpent)
    {
        Date = date;
        TimeSpent = timeSpent;
    }
}