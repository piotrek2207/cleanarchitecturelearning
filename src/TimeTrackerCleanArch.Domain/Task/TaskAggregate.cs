﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared.CommonExtensions;
using Shared.CommonExtensions.ObjectExtensions;
using Shared.Domain.Common;
using Shared.Domain.Common.Exceptions;
using TimeTrackerCleanArch.Domain.Task.Events;
using TimeTrackerCleanArch.Domain.Task.Events.Dto;
using TimeTrackerCleanArch.Domain.Task.Events.Dto.Extensions;

namespace TimeTrackerCleanArch.Domain.Task;

public class TaskAggregate : Aggregate
{
    public string Title { get; private set; }
    public string Description { get; private set; }
    public TaskStatusEnum Status { get; private set; }
    public Guid ProjectId { get; private set; }
    public TimeSpan TimeRemaining { get; private set; }
    public IReadOnlyCollection<TaskWorkLog> WorkLogs { get; private set; }

    protected TaskAggregate() { }

    public TaskAggregate(
        Guid id,
        string title,
        string description,
        TaskStatusEnum status,
        Guid projectId,
        TimeSpan timeRemaining,
        IReadOnlyCollection<TaskWorkLog> workLogs)
        : base(id)
    {
        var workLogDetails = workLogs?
            .Select(wl => new TaskWorkLogDetails(
                id: wl.Id,
                date: wl.Date,
                timeSpent: wl.TimeSpent))
            .ToList();

        var @event = new TaskCreated(
            aggregateId: id,
            title: title,
            description: description,
            status: status,
            projectId: projectId,
            timeRemaining: timeRemaining,
            workLogs: workLogDetails);

        Apply(@event);
        Enqueue(@event);
    }

    public void UpdateSummary(string title, string description)
    {
        var @event = new TaskSummaryChanged(Id, title, description);

        Apply(@event);
        Enqueue(@event);
    }

    public void StartProgress()
    {
        AssertStatusIn(TaskStatusEnum.ToDo, TaskStatusEnum.Reopened);

        UpdateStatus(TaskStatusEnum.InProgress);
    }

    public void MarkAsDone()
    {
        AssertStatusIn(TaskStatusEnum.ToDo, TaskStatusEnum.InProgress, TaskStatusEnum.Reopened);

        UpdateStatus(TaskStatusEnum.Done);
    }

    public void Cancel()
    {
        AssertStatusIn(TaskStatusEnum.ToDo, TaskStatusEnum.InProgress, TaskStatusEnum.Reopened);

        UpdateStatus(TaskStatusEnum.Cancelled);
    }

    public void Reopen()
    {
        AssertStatusIn(TaskStatusEnum.Done, TaskStatusEnum.Cancelled);

        UpdateStatus(TaskStatusEnum.Reopened);
    }

    public void AddWorkLog(Guid workLogId, DateTime date, TimeSpan timeSpent)
    {
        if(WorkLogs.Any(wl => wl.Id == workLogId) is true)
            throw new DomainException($"WorkLog with Id {workLogId} already exists within the task.");

        var newWorkLog = new TaskWorkLogDetails(
            id: workLogId,
            date: date,
            timeSpent: timeSpent);

        var @event = new TaskWorkLogAdded(Id, newWorkLog);

        Apply(@event);
        Enqueue(@event);
    }

    public void UpdateWorkLog(Guid workLogId, DateTime date, TimeSpan timeSpent)
    {
        if (WorkLogs.All(w => w.Id != workLogId))
            throw new DomainException($"Work Log with aggregateId {workLogId} doesn't exist");

        var updatedWorkLog = new TaskWorkLogDetails(
            id: workLogId,
            date: date,
            timeSpent: timeSpent);

        var @event = new TaskWorkLogChanged(Id, updatedWorkLog);

        Apply(@event);
        Enqueue(@event);
    }

    public void RemoveWorkLog(Guid workLogId)
    {
        var workLog = WorkLogs.FirstOrDefault(w => w.Id == workLogId);
        if (workLog is null)
            throw new DomainException($"Work Log with aggregateId {workLogId} doesn't exist");

        var details = new TaskWorkLogDetails(
            workLog.Id,
            workLog.Date,
            workLog.TimeSpent);

        var @event = new TaskWorkLogRemoved(
            aggregateId: Id,
            workLogDetails: details);

        Apply(@event);
        Enqueue(@event);
    }

    private void UpdateStatus(TaskStatusEnum newStatus)
    {
        if (newStatus == Status)
            throw new DomainException($"Task is already in status {newStatus}");

        var @event = new TaskStatusChanged(Id, Status, newStatus);

        Apply(@event);
        Enqueue(@event);
    }

    private void Apply(TaskCreated @event)
    {
        Title = @event.Title;
        Description = @event.Description;
        Status = @event.Status;
        ProjectId = @event.ProjectId;
        TimeRemaining = @event.TimeRemaining;
        WorkLogs = @event.WorkLogs?.Select(wl => wl.ToDomainEntity()).ToList() ?? new List<TaskWorkLog>();
    }

    private void Apply(TaskSummaryChanged @event)
    {
        Title = @event.Title;
        Description = @event.Description;
    }

    private void Apply(TaskStatusChanged @event)
    {
        Status = @event.NewStatus;
    }

    private void Apply(TaskWorkLogAdded @event)
    {
        var newWorkLogs = WorkLogs.ToList();
        newWorkLogs.Add(@event.WorkLogDetails.ToDomainEntity());
        WorkLogs = newWorkLogs;
    }

    private void Apply(TaskWorkLogChanged @event)
    {
        var workLog = WorkLogs.First(x => x.Id == @event.WorkLogDetails.Id);

        workLog.Update(
            @event.WorkLogDetails.Date,
            @event.WorkLogDetails.TimeSpent);
    }

    private void Apply(TaskWorkLogRemoved @event)
    {
        var newWorkLogs = WorkLogs.ToList();
        newWorkLogs.RemoveAll(l => l.Id == @event.WorkLogDetails.Id);
        WorkLogs = newWorkLogs;
    }

    private void AssertStatusIn(params TaskStatusEnum[] statuses)
    {
        if (Status.NotIn(statuses))
        {
            throw new DomainException($"Only {statuses.ToCommaSeparatedList()} tasks can " +
                                      $"be marked as {TaskStatusEnum.Reopened}");
        }
    }
}

public enum TaskStatusEnum
{
    ToDo,
    Reopened,
    InProgress,
    Done,
    Cancelled
}