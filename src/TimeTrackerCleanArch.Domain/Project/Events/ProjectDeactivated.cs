﻿using System;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Domain.Project.Events;

public class ProjectDeactivated : AggregateEvent
{
    public ProjectDeactivated(Guid aggregateId) : base(aggregateId)
    {
    }
}