﻿using System;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Domain.Project.Events;

public class ProjectActivated : AggregateEvent
{
    public ProjectActivated(Guid aggregateId) : base(aggregateId)
    {
    }
}