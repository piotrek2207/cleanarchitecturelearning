﻿using System;
using Shared.Domain.Common;

namespace TimeTrackerCleanArch.Domain.Project.Events;

public class ProjectCreated : AggregateEvent
{
    public string Name { get; private set; }
    public string Description { get; private set; }

    public ProjectCreated(Guid aggregateId, string name, string description) : base(aggregateId)
    {
        Name = name;
        Description = description;
    }
}