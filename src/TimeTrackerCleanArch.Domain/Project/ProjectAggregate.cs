﻿using System;
using Shared.Domain.Common;
using Shared.Domain.Common.Exceptions;
using TimeTrackerCleanArch.Domain.Project.Events;

namespace TimeTrackerCleanArch.Domain.Project;

public class ProjectAggregate : Aggregate
{
    public string Name { get; private set; }
    public string Description { get; private set; }
    public bool IsActive { get; private set; }

    protected ProjectAggregate() { }

    public ProjectAggregate(Guid id, string name, string description) : base(id)
    {
        var @event = new ProjectCreated(
            aggregateId: id, 
            name: name, 
            description: description);

        Enqueue(@event);
        Apply(@event);
    }

    public void Deactivate()
    {
        if (IsActive == false)
            throw new DomainException("Project is already inactive.");

        var @event = new ProjectDeactivated(Id);
            
        Enqueue(@event);
        Apply(@event);
    }

    public void Activate()
    {
        if (IsActive == true)
            throw new DomainException("Project is already active.");

        var @event = new ProjectActivated(Id);

        Enqueue(@event);
        Apply(@event);
    }

    private void Apply(ProjectCreated @event)
    {
        Id = @event.AggregateId;
        Name = @event.Name;
        Description = @event.Description;
        IsActive = true;
    }

    private void Apply(ProjectDeactivated @event)
    {
        IsActive = false;
    }

    private void Apply(ProjectActivated @event)
    {
        IsActive = true;
    }
}