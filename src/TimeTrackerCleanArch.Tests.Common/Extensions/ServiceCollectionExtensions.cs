﻿using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace TimeTrackerCleanArch.Tests.Common.Extensions;

public static class ServiceCollectionExtensions
{
    public static void RemoveImplementationsOfType<T>(this ServiceCollection services)
    {
        var descriptor = services.SingleOrDefault(
            d => d.ServiceType == typeof(T));

        if (descriptor != null)
        {
            services.Remove(descriptor);
        }
    }
}