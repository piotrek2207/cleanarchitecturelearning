﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared.TestsInfrastructure.Generators;
using Shared.TestsInfrastructure.Infrastructure;
using TimeTrackerCleanArch.Domain.Task;

namespace TimeTrackerCleanArch.Tests.Common.Builders.Tasks;

public class TaskBuilder : AggregateBuilder<TaskAggregate>
{
    public Guid Id { get; private set; }
    public string Title { get; private set; }
    public string Description { get; private set; }
    public TaskStatusEnum Status { get; private set; }
    public Guid ProjectId { get; private set; }
    public TimeSpan TimeRemaining { get; private set; }
    public IReadOnlyCollection<TaskWorkLogBuilder> WorkLogs { get; private set; }

    public TaskBuilder()
    {
        Id = IdGenerator.GenerateId();
        Title = StringGenerator.GenerateWord();
        Description = StringGenerator.GenerateSentence();
        Status = TaskStatusEnum.ToDo;
        ProjectId = IdGenerator.GenerateId();
        TimeRemaining = TimeSpan.FromDays(3);
        WorkLogs = null;
    }

    public TaskBuilder WithStatus(TaskStatusEnum status)
    {
        Status = status;
        return this;
    }

    public TaskBuilder WithWorkLogs(params Func<TaskWorkLogBuilder, TaskWorkLogBuilder>[] builders)
    {
        WorkLogs = builders?.Select(builder => builder(new TaskWorkLogBuilder())).ToList();
        return this;
    }

    public TaskBuilder WithEmptyWorkLogs()
    {
        WorkLogs = new List<TaskWorkLogBuilder>();
        return this;
    }

    protected override TaskAggregate BuildAggregate()
    {
        return new TaskAggregate(
            id: Id,
            title: Title,
            description: Description,
            status: Status,
            projectId: ProjectId,
            timeRemaining: TimeRemaining,
            workLogs: WorkLogs?.Select(b => b.Build()).ToList());
    }
}