﻿using System;
using Shared.TestsInfrastructure.Generators;
using Shared.TestsInfrastructure.Infrastructure;
using TimeTrackerCleanArch.Domain.Task;

namespace TimeTrackerCleanArch.Tests.Common.Builders.Tasks;

public class TaskWorkLogBuilder : ObjectBuilder<TaskWorkLog>
{
    public Guid Id { get; private set; }
    public DateTime Date { get; private set; }
    public TimeSpan TimeSpent { get; private set; }

    public TaskWorkLogBuilder()
    {
        Id = IdGenerator.GenerateId();
        Date = DateTimeGenerator.PastDate();
        TimeSpent = TimeSpanGenerator.GenerateRandomHours(maxHours: 8);
    }

    public TaskWorkLogBuilder WithId(Guid id)
    {
        Id = id;
        return this;
    }

    public TaskWorkLogBuilder WithDate(DateTime date)
    {
        Date = date;
        return this;
    }

    public TaskWorkLogBuilder WithTimeSpent(TimeSpan timeSpent)
    {
        TimeSpent = timeSpent;
        return this;
    }

    public override TaskWorkLog Build()
    {
        return new TaskWorkLog(
            id: Id,
            date: Date,
            timeSpent: TimeSpent);
    }
}