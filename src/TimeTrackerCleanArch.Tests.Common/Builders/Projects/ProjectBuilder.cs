﻿using System;
using Shared.TestsInfrastructure.Generators;
using Shared.TestsInfrastructure.Infrastructure;
using TimeTrackerCleanArch.Domain.Project;

namespace TimeTrackerCleanArch.Tests.Common.Builders.Projects;

public class ProjectBuilder : AggregateBuilder<ProjectAggregate>
{
    public Guid AggregateId { get; private set; }
    public string Name { get; private set; }
    public string Description { get; private set; }
    public bool IsActive { get; private set; }

    public ProjectBuilder()
    {
        AggregateId = IdGenerator.GenerateId();
        Name = StringGenerator.GenerateWord();
        Description = StringGenerator.GenerateSentence();
        IsActive = true;
    }

    public ProjectBuilder WithIsActive(bool isActive)
    {
        IsActive = false;
        return this;
    }

    protected override ProjectAggregate BuildAggregate()
    {
        var project = new ProjectAggregate(
            id: AggregateId,
            name: Name,
            description: Description);

        if (!IsActive)
        {
            project.Deactivate();
        }

        return project;
    }
}