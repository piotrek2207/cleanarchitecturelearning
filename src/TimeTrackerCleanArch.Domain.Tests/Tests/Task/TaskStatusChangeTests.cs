﻿using FluentAssertions;
using Shared.Domain.Common.Exceptions;
using Shared.TestsInfrastructure.Extensions;
using TimeTrackerCleanArch.Domain.Task;
using TimeTrackerCleanArch.Domain.Task.Events;
using TimeTrackerCleanArch.Tests.Common.Builders.Tasks;
using Xunit;

namespace TimeTrackerCleanArch.Domain.Tests.Tests.Task;

public class TaskStatusChangeTests
{
    [Theory]
    [InlineData(TaskStatusEnum.ToDo)]
    [InlineData(TaskStatusEnum.Reopened)]
    public void StartProgress_WhenCorrectInitialStatus_ShouldMarkAsInProgress(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        task.StartProgress();

        // Assert
        var expectedNewStatus = TaskStatusEnum.InProgress;
        AssertOnlyStatusChanged(initialTask, task, expectedNewStatus);
    }

    [Theory]
    [InlineData(TaskStatusEnum.InProgress)]
    [InlineData(TaskStatusEnum.Cancelled)]
    [InlineData(TaskStatusEnum.Done)]
    public void StartProgress_WhenIncorrectInitialStatus_ShouldThrowDomainException(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        var exception = Record.Exception(() => task.StartProgress());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain($"can be marked");
        AssertNotChanged(initialTask, task);
    }

    [Theory]
    [InlineData(TaskStatusEnum.ToDo)]
    [InlineData(TaskStatusEnum.Reopened)]
    [InlineData(TaskStatusEnum.InProgress)]
    public void Cancel_WhenCorrectInitialStatus_ShouldMarkAsCancelled(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        task.Cancel();

        // Assert
        var expectedNewStatus = TaskStatusEnum.Cancelled;
        AssertOnlyStatusChanged(initialTask, task, expectedNewStatus);
    }

    [Theory]
    [InlineData(TaskStatusEnum.Done)]
    [InlineData(TaskStatusEnum.Cancelled)]
    public void Cancel_WhenIncorrectInitialStatus_ShouldThrowDomainException(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        var exception = Record.Exception(() => task.Cancel());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("can be marked");
        AssertNotChanged(initialTask, task);
    }

    [Theory]
    [InlineData(TaskStatusEnum.ToDo)]
    [InlineData(TaskStatusEnum.Reopened)]
    [InlineData(TaskStatusEnum.InProgress)]
    public void MarkAsDone_WhenCorrectInitialStatus_ShouldMarkAsDone(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        task.MarkAsDone();

        // Assert
        var expectedNewStatus = TaskStatusEnum.Done;
        AssertOnlyStatusChanged(initialTask, task, expectedNewStatus);
    }

    [Theory]
    [InlineData(TaskStatusEnum.Done)]
    [InlineData(TaskStatusEnum.Cancelled)]
    public void MarkAsDone_WhenIncorrectInitialStatus_ShouldThrowDomainException(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        var exception = Record.Exception(() => task.MarkAsDone());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("can be marked");
        AssertNotChanged(initialTask, task);
    }

    [Theory]
    [InlineData(TaskStatusEnum.Cancelled)]
    [InlineData(TaskStatusEnum.Done)]
    public void Reopen_WhenCorrectInitialStatus_ShouldMarkAsReopened(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        task.Reopen();

        // Assert
        var expectedNewStatus = TaskStatusEnum.Reopened;
        AssertOnlyStatusChanged(initialTask, task, expectedNewStatus);
    }

    [Theory]
    [InlineData(TaskStatusEnum.ToDo)]
    [InlineData(TaskStatusEnum.InProgress)]
    [InlineData(TaskStatusEnum.Reopened)]
    public void Reopen_WhenIncorrectInitialStatus_ShouldThrowDomainException(TaskStatusEnum initialStatus)
    {
        // Arrange
        var taskBuilder = new TaskBuilder()
            .WithStatus(initialStatus);
        var initialTask = taskBuilder.Build();
        var task = taskBuilder.Build();

        // Act
        var exception = Record.Exception(() => task.Reopen());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain($"can be marked");
        AssertNotChanged(initialTask, task);
    }

    private void AssertOnlyStatusChanged(TaskAggregate initial, TaskAggregate actual, TaskStatusEnum expectedNewStatus)
    {
        actual.Status.Should().Be(expectedNewStatus);
        actual.Version.Should().BeGreaterThan(initial.Version);
        actual.ShouldBeEquivalentTo(initial, config => config
            .Excluding(x => x.Status)
            .Excluding(x => x.Version));
        actual.Should().EnqueueSingleEvent<TaskStatusChanged>(with =>
            with.PreviousStatus == initial.Status
            && with.NewStatus == expectedNewStatus);
    }

    private void AssertNotChanged(TaskAggregate initial, TaskAggregate actual)
    {
        actual.ShouldBeEquivalentTo(initial);
        actual.Should().NotEnqueueAnyEvents();
    }
}