﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Shared.TestsInfrastructure.Extensions;
using Shared.TestsInfrastructure.Generators;
using TimeTrackerCleanArch.Domain.Task;
using TimeTrackerCleanArch.Domain.Task.Events;
using TimeTrackerCleanArch.Tests.Common.Builders.Tasks;
using Xunit;

namespace TimeTrackerCleanArch.Domain.Tests.Tests.Task;

public class TaskAggregateTests
{
    [Fact]
    public void Create_WhenInvoked_ShouldCreateTask()
    {
        // Arrange
        var id = IdGenerator.GenerateId();
        var title = StringGenerator.GenerateWord();
        var description = StringGenerator.GenerateSentence();
        var status = TaskStatusEnum.ToDo;
        var projectId = IdGenerator.GenerateId();
        var timeRemaining = TimeSpan.FromDays(2);

        var workLogId = Guid.NewGuid();
        var workLogDate = new DateTime(2021, 02, 28);
        var workLogTimeSpent = TimeSpan.FromHours(3.75);

        // Act
        var task = new TaskAggregate(
            id: id,
            title: title,
            description: description,
            status: status,
            projectId: projectId,
            timeRemaining: timeRemaining,
            workLogs: new List<TaskWorkLog>
            {
                new TaskWorkLog(
                    id: workLogId,
                    date: workLogDate,
                    timeSpent: workLogTimeSpent)
            });

        // Assert
        task.Id.Should().Be(id);
        task.Title.Should().Be(title);
        task.Description.Should().Be(description);
        task.Status.Should().Be(status);
        task.ProjectId.Should().Be(projectId);
        task.TimeRemaining.Should().Be(timeRemaining);
            
        var workLog = task.WorkLogs.Should().ContainSingle().Subject;
        workLog.Id.Should().Be(workLogId);
        workLog.Date.Should().Be(workLogDate);
        workLog.TimeSpent.Should().Be(workLogTimeSpent);

        task.Should().EnqueueSingleEvent<TaskCreated>(with =>
            with.Title == title
            && with.Description == description
            && with.Status == status
            && with.ProjectId == projectId
            && with.TimeRemaining == timeRemaining
            && with.WorkLogs.Count == 1
            && with.WorkLogs.Any(wl =>
                wl.Id == workLogId
                && wl.Date == workLogDate
                && wl.TimeSpent == workLogTimeSpent));
    }

    [Fact]
    public void UpdateSummary_WhenInvoked_ShouldUpdateSummary()
    {
        // Arrange
        var task = new TaskBuilder().Build();
        var newTitle = StringGenerator.GenerateWord();
        var newDescription = StringGenerator.GenerateSentence();

        // Act
        task.UpdateSummary(
            title: newTitle,
            description: newDescription);

        // Assert
        task.Title.Should().Be(newTitle);
        task.Description.Should().Be(newDescription);

        task.Should().EnqueueSingleEvent<TaskSummaryChanged>(with =>
            with.Title == newTitle
            && with.Description == newDescription);
    }
}