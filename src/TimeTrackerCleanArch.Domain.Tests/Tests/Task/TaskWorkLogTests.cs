﻿using System;
using FluentAssertions;
using Shared.CommonExtensions.ObjectExtensions;
using Shared.Domain.Common.Exceptions;
using TimeTrackerCleanArch.Tests.Common.Builders.Tasks;
using Shared.TestsInfrastructure.Extensions;
using TimeTrackerCleanArch.Domain.Task.Events;
using Xunit;

namespace TimeTrackerCleanArch.Domain.Tests.Tests.Task;

public class TaskWorkLogTests
{
    [Fact]
    public void Add_WhenNotExists_ShouldAdd()
    {
        // Arrange
        var task = new TaskBuilder().Build();
        var workLogId = Guid.NewGuid();
        var date = new DateTime(2021, 01, 05);
        var timeSpent = TimeSpan.FromHours(2.5);

        // Act
        task.AddWorkLog(workLogId, date, timeSpent);

        // Assert
        var workLog = task.WorkLogs.Should().ContainSingle().Subject;
        workLog.Id.Should().Be(workLogId);
        workLog.Date.Should().Be(date);
        workLog.TimeSpent.Should().Be(timeSpent);

        task.Should().EnqueueSingleEvent<TaskWorkLogAdded>(with =>
            with.WorkLogDetails.Id == workLogId
            && with.WorkLogDetails.Date == date
            && with.WorkLogDetails.TimeSpent == timeSpent);
    }

    [Fact]
    public void Add_WhenIdAlreadyExists_ShouldThrowDomainException()
    {
        // Arrange
        var workLogId = Guid.NewGuid();
        var date = new DateTime(2021, 01, 05);
        var timeSpent = TimeSpan.FromHours(2.5);

        var task = new TaskBuilder()
            .WithWorkLogs(
                builder => builder.WithId(workLogId))
            .Build();

        // Act
        var exception = Record.Exception(() => task.AddWorkLog(workLogId, date, timeSpent));

        // Assert
        exception.Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain($"already exists");
        task.Should().NotEnqueueAnyEvents();
    }

    [Fact]
    public void Update_WhenExists_ShouldUpdate()
    {
        // Arrange
        var workLogId = Guid.NewGuid();
        var initialWorkLogDate = new DateTime(2021, 05, 02);
        var initialWorkLogTimeSpent = TimeSpan.FromHours(2);
        var updatedWorkLogDate = new DateTime(2022, 07, 25);
        var updatedWorkLogTimeSpent = TimeSpan.FromHours(3.5);

        var task = new TaskBuilder()
            .WithWorkLogs(builder => builder
                .WithId(workLogId)
                .WithDate(initialWorkLogDate)
                .WithTimeSpent(initialWorkLogTimeSpent))
            .Build();

        // Act
        task.UpdateWorkLog(workLogId, updatedWorkLogDate, updatedWorkLogTimeSpent);

        // Assert
        var workLog = task.WorkLogs.Should().ContainSingle().Subject;
        workLog.Id.Should().Be(workLogId);
        workLog.Date.Should().Be(updatedWorkLogDate);
        workLog.TimeSpent.Should().Be(updatedWorkLogTimeSpent);

        task.Should().EnqueueSingleEvent<TaskWorkLogChanged>(with =>
            with.WorkLogDetails != null
            && with.WorkLogDetails.Id == workLogId
            && with.WorkLogDetails.Date == updatedWorkLogDate
            && with.WorkLogDetails.TimeSpent == updatedWorkLogTimeSpent);
    }

    [Fact]
    public void Update_WhenNotExists_ShouldThrowDomainException()
    {
        // Arrange
        var workLog1Id = Guid.NewGuid();
        var workLog2Id = Guid.NewGuid();
        var updatedWorkLogId = Guid.NewGuid();
        var updatedWorkLogDate = new DateTime(2022, 07, 25);
        var updatedWorkLogTimeSpent = TimeSpan.FromHours(3.5);

        var task = new TaskBuilder()
            .WithWorkLogs(
                builder => builder.WithId(workLog1Id),
                builder => builder.WithId(workLog2Id))
            .Build();

        // Act
        var exception = Record.Exception(() =>
            task.UpdateWorkLog(updatedWorkLogId, updatedWorkLogDate, updatedWorkLogTimeSpent));


        // Assert
        task.WorkLogs.Should().OnlyContain(wl => wl.Id.In(workLog1Id, workLog2Id));
        exception.Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("doesn't exist");
        task.Should().NotEnqueueAnyEvents();
    }

    [Fact]
    public void Delete_WhenExists_ShouldDelete()
    {
        // Arrange
        var workLog1Id = Guid.NewGuid();
        var workLog1Date = new DateTime(2021, 03, 05);
        var workLog1TimeSpent = TimeSpan.FromHours(2);

        var workLog2Id = Guid.NewGuid();
        var workLog2Date = new DateTime(2021, 02, 01);
        var workLog2TimeSpent = TimeSpan.FromHours(4.5);
        var task = new TaskBuilder()
            .WithWorkLogs(
                builder => builder.WithId(workLog1Id).WithDate(workLog1Date).WithTimeSpent(workLog1TimeSpent),
                builder => builder.WithId(workLog2Id).WithDate(workLog2Date).WithTimeSpent(workLog2TimeSpent))
            .Build();

        // Act
        task.RemoveWorkLog(workLog1Id);

        // Assert
        var remainingWorkLog = task.WorkLogs.Should().ContainSingle().Subject;
        remainingWorkLog.Id.Should().Be(workLog2Id);
        remainingWorkLog.Date.Should().Be(workLog2Date);
        remainingWorkLog.TimeSpent.Should().Be(workLog2TimeSpent);

        task.Should().EnqueueSingleEvent<TaskWorkLogRemoved>(with =>
            with.WorkLogDetails.Id == workLog1Id
            && with.WorkLogDetails.Date == workLog1Date
            && with.WorkLogDetails.TimeSpent == workLog1TimeSpent);
    }

    [Fact]
    public void Delete_WhenNotExists_ShouldThrowDomainException()
    {
        // Arrange
        var notExistingWorkLogId = Guid.NewGuid();

        var workLog1Id = Guid.NewGuid();
        var workLog1Date = new DateTime(2021, 03, 05);
        var workLog1TimeSpent = TimeSpan.FromHours(2);

        var workLog2Id = Guid.NewGuid();
        var workLog2Date = new DateTime(2021, 02, 01);
        var workLog2TimeSpent = TimeSpan.FromHours(4.5);
        var task = new TaskBuilder()
            .WithWorkLogs(
                builder => builder.WithId(workLog1Id).WithDate(workLog1Date).WithTimeSpent(workLog1TimeSpent),
                builder => builder.WithId(workLog2Id).WithDate(workLog2Date).WithTimeSpent(workLog2TimeSpent))
            .Build();

        // Act
        var exception = Record.Exception(() => task.RemoveWorkLog(notExistingWorkLogId));

        // Assert
        task.WorkLogs.Should().OnlyContain(wl => wl.Id.In(workLog1Id, workLog2Id));
        exception.Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("doesn't exist");
        task.Should().NotEnqueueAnyEvents();
    }
}