﻿using System;
using FluentAssertions;
using Shared.Domain.Common.Exceptions;
using Shared.TestsInfrastructure.Extensions;
using Shared.TestsInfrastructure.Generators;
using TimeTrackerCleanArch.Domain.Project;
using TimeTrackerCleanArch.Domain.Project.Events;
using TimeTrackerCleanArch.Tests.Common.Builders.Projects;
using Xunit;

namespace TimeTrackerCleanArch.Domain.Tests.Tests.Project;

public class ProjectAggregateTests
{
    [Fact]
    public void Create_WhenInvoked_ShouldCreateProject()
    {
        // Arrange
        var id = Guid.NewGuid();
        var name = StringGenerator.GenerateWord();
        var description = StringGenerator.GenerateSentence();

        // Act
        var project = new ProjectAggregate(
            id: id,
            name: name,
            description: description);

        // Assert
        project.Id.Should().Be(id);
        project.Name.Should().Be(name);
        project.Description.Should().Be(description);
        project.IsActive.Should().BeTrue();

        project.Should().EnqueueSingleEvent<ProjectCreated>(with =>
            with.Description == description
            && with.Name == name);
    }

    [Fact]
    public void Deactivate_WhenIsActive_ShouldDeactivate()
    {
        // Arrange
        var project = new ProjectBuilder().Build();

        // Act
        project.Deactivate();

        // Assert
        project.IsActive.Should().BeFalse();
        project.Should().EnqueueSingleEvent<ProjectDeactivated>();
    }

    [Fact]
    public void Deactivate_WhenIsInactive_ShouldThrowDomainException()
    {
        // Arrange
        var project = new ProjectBuilder()
            .WithIsActive(false)
            .Build();

        // Act
        var exception = Record.Exception(() => project.Deactivate());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("already inactive");
        project.Should().NotEnqueueAnyEvents();
    }

    [Fact]
    public void Activate_WhenIsNotActive_ShouldActivate()
    {
        // Arrange
        var project = new ProjectBuilder()
            .WithIsActive(false)
            .Build();

        // Act
        project.Activate();

        // Assert
        project.IsActive.Should().BeTrue();
        project.Should().EnqueueSingleEvent<ProjectActivated>();
    }

    [Fact]
    public void Activate_WhenIsActive_ShouldThrowDomainException()
    {
        // Arrange
        var project = new ProjectBuilder().Build();

        // Act
        var exception = Record.Exception(() => project.Activate());

        // Assert
        exception
            .Should().BeOfType<DomainException>()
            .Which.Message.Should().Contain("already active");
        project.Should().NotEnqueueAnyEvents();
    }
}