﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using FluentAssertions.Primitives;
using Shared.Domain.Common;

namespace Shared.TestsInfrastructure.Extensions;

public class AggregateEventAssertions<TAggregate> : ObjectAssertions
    where TAggregate : Aggregate
{
    private readonly TAggregate _aggregate;
    private readonly List<IAggregateEvent> _aggregateEvents;

    public AggregateEventAssertions(TAggregate aggregate, List<IAggregateEvent> aggregateEvents)
        :base(aggregate)
    {
        _aggregate = aggregate;
        _aggregateEvents = aggregateEvents;
    }

    public AndWhichConstraint<AggregateEventAssertions<TAggregate>, TEvent> EnqueueEvent<TEvent>(
        Func<TEvent, bool> additionalConditions = null)
        where TEvent : IAggregateEvent
    {
        var @event = (TEvent)_aggregateEvents.Should()
            .Contain(e =>
                e.AggregateId == _aggregate.Id
                && e.GetType() == typeof(TEvent)
                && (additionalConditions == null || additionalConditions((TEvent)e)))
            .Subject;

        return new AndWhichConstraint<AggregateEventAssertions<TAggregate>, TEvent>(this, @event);
    }

    public AndWhichConstraint<AggregateEventAssertions<TAggregate>, TEvent> EnqueueSingleEvent<TEvent>(
        Func<TEvent, bool> additionalConditions = null)
        where TEvent : IAggregateEvent
    {
        _aggregateEvents.Should().HaveCount(1);
        return EnqueueEvent(additionalConditions);
    }

    public AndConstraint<AggregateEventAssertions<TAggregate>> NotEnqueueAnyEvents()
    {
        _aggregateEvents.Should().BeNullOrEmpty();

        return new AndConstraint<AggregateEventAssertions<TAggregate>>(this);
    }
}