﻿using System.Linq;
using Shared.Domain.Common;

namespace Shared.TestsInfrastructure.Extensions;

public static class FluentAssertionExtensions
{
    public static AggregateEventAssertions<TAggregate> Should<TAggregate>(this TAggregate aggregate)
        where TAggregate : Aggregate
    {
        return new AggregateEventAssertions<TAggregate>(
            aggregate, 
            aggregate.DequeueAllUncommittedEvents().ToList());
    }
}