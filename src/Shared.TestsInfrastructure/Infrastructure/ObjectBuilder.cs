﻿namespace Shared.TestsInfrastructure.Infrastructure;

public abstract class ObjectBuilder<T>
{
    public abstract T Build();
}