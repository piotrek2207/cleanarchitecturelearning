﻿using Shared.Domain.Common;

namespace Shared.TestsInfrastructure.Infrastructure;

public abstract class AggregateBuilder<T> : ObjectBuilder<T>
    where T : Aggregate
{
    public override T Build()
    {
        var aggregate = BuildAggregate();
        aggregate.DequeueAllUncommittedEvents();
        return aggregate;
    }

    protected abstract T BuildAggregate();
}