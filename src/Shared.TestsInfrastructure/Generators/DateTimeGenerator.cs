﻿using System;
using Bogus;

namespace Shared.TestsInfrastructure.Generators;

public static class DateTimeGenerator
{
    private static readonly Faker Faker = new Faker();

    public static DateTime PastDate()
    {
        return Faker.Date.Past().Date;
    }
}