﻿using Bogus;

namespace Shared.TestsInfrastructure.Generators;

public static class StringGenerator
{
    private static readonly Faker Faker = new Faker();

    public static string GenerateWord()
    {
        return Faker.Random.Word();
    }

    public static string GenerateSentence(int wordsCount = 10)
    {
        return $"{Faker.Random.Words(wordsCount)}.";
    }
}