﻿using System;
using Bogus;

namespace Shared.TestsInfrastructure.Generators;

public static class IdGenerator
{
    private static readonly Faker Faker = new Faker();

    public static Guid GenerateId()
    {
        return Faker.Random.Guid();
    }
}