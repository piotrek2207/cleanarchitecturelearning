﻿using System;
using Bogus;

namespace Shared.TestsInfrastructure.Generators;

public static class TimeSpanGenerator
{
    private static readonly Faker Faker = new Faker();

    public static TimeSpan GenerateRandomHours(double maxHours)
    {
        var randomHours = Faker.Random.Double(max: maxHours);
        var randomHoursRounded = Math.Round(randomHours, 1);
        return TimeSpan.FromHours(randomHoursRounded);
    }
}