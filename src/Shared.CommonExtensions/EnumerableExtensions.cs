﻿using System.Collections.Generic;

namespace Shared.CommonExtensions;

public static class EnumerableExtensions
{
    public static string ToCommaSeparatedList<T>(this IEnumerable<T> source)
    {
        if (source == null)
            return string.Empty;

        return string.Join(", ", source);
    }
}