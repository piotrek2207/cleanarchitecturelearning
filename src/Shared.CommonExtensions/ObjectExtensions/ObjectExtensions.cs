﻿using System.Linq;

namespace Shared.CommonExtensions.ObjectExtensions;

public static class ObjectExtensions
{
    public static bool In<T>(this T item, params T[] arguments)
    {
        if (arguments?.Any() != true)
            return false;

        return arguments.Contains(item);
    }

    public static bool NotIn<T>(this T item, params T[] arguments)
    {
        return !item.In(arguments);
    }
}