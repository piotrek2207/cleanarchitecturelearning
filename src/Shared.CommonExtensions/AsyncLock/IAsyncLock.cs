﻿using System;
using System.Threading.Tasks;

namespace Shared.CommonExtensions.AsyncLock;

public interface IAsyncLock
{
    Task AcquireFor(Func<Task> action);
}