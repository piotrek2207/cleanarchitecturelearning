﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.CommonExtensions.AsyncLock;

public class InMemoryAsyncLock : IAsyncLock
{
    private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

    public async Task AcquireFor(Func<Task> action)
    {
        await _semaphore.WaitAsync();

        try
        {
            await action();
        }
        finally
        {
            _semaphore.Release();
        }
    }
}